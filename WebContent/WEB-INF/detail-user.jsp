<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="header.jsp"%>
<meta charset="UTF-8">
<title>利用者詳細</title>
<style>
.title {
  color: #17a2b8;
  border-radius: 10px;
  padding: 16px;
  margin-top: 16px;
}
.bg-color{
    background:#F5FFFA;
}
 .table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
    background-color: #F5FFFA;
}
.bg-light {
	background-color: #F5FFFA!important;
}
</style>
</head>
<body>
	<%@ include file="manager-operator-navbar.jsp"%>
	<div class="container">
		<p class="h1 bg-light title text-info rounded">
					<c:out value="${userDto.name}" />
					さんの学習履歴
				</p>
		<br>
		<form action="list-user" method="get">
			<button type="submit" class="btn btn-primary">一覧に戻る</button>
		</form>
		<br>
				<c:choose>
			<c:when test="${ userDto.managerId eq manager.managerId }">
		
		<table class="table table-hover">
				<tr class="text-center bg-color">
					<th class="text-secondary">日付</th>
					<th class="text-secondary">カテゴリ名</th>
					<th class="text-secondary">コース名</th>
					<th class="text-secondary">状況</th>
				</tr>
			<tbody>
				<c:forEach items="${StudyHistoryList}" var="StudyHistory">
					<tr align="center">
						<td><c:out value="${StudyHistory.lastStudyTime}"></c:out></td>
						<td><c:out value="${StudyHistory.categoryName}"></c:out></td>
						<td><c:out value="${StudyHistory.courseName}"></c:out></td>
						<td><c:if
								test="${StudyHistory.maxPage == StudyHistory.totalPages }"
								var="History"></c:if> <c:if test="${History}">
					受講しました
					</c:if> <c:if test="${!History}">
								<c:out value="${StudyHistory.maxPage}"></c:out>ページ/<c:out
									value="${StudyHistory.totalPages}"></c:out>ページ
					</c:if></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		</c:when>
		<c:otherwise>アクセス権限がありません</c:otherwise>
		</c:choose>
	</div>
</body>
</html>