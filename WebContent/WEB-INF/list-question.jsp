<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<% pageContext.setAttribute("newLineChar", "\n"); %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>質問掲示板</title>
<%@ include file="header.jsp"%>
<style type="text/css">
	<!--
		h1 { color:#330066 !important;
			 background: linear-gradient(-135deg, #DCDCDC, #EAD9FF) !important; }
		h3 { color:#330066 !important;
			 background: linear-gradient(-135deg, #DCDCDC, #EAD9FF) !important; }
		.card-header { color:#330066 !important;
					   background : linear-gradient(-135deg, #DCDCDC, #EAD9FF) !important;
					   padding : 3px 5px 3px !important; }
	-->
</style>
</head>
<body>
<%@ include file="navbar.jsp"%>
	<!-- メッセージの色変更　by古瀬 -->
	<div class="text-center">
		<p class="mx-auto text-danger">
			<c:out value="${ message }" />
		</p>
		<div class="row">
			<div class="mx-auto text-primary">
				&nbsp;<c:out value="${ successMessage }" />
			</div>
		</div>
	</div>
	<div class="mx-auto clearfix" style="width: 70%;">
			<c:forEach items="${ questionList }" var="questionDto" varStatus="status">
			<c:if test="${ status.index eq 0 }">
			<h1 class="mt-3 p-3 bg-light rounded">
			<c:out value="${ questionDto.courseName }"/>の質問掲示板
			</h1>
			<br><br>
			</c:if>
				<div class="mt-1 mb-2 card mx-auto">
				<div class="card-header">
				<div style="width: 30%; float: left;">
				<p class="my-auto h5 card-text">
					質問
					<c:out value="${status.index+1}" />
				</p>
				</div>
				<div class="my-auto text-right" style="width: 70%; float: left">
						質問日時：
						<c:out value="${questionDto.time }" />
				</div>
			    </div>
			    <div class="card-body">
			    	<c:forEach var="line" items="${fn:split(questionDto.question, newLineChar)}" >
						<c:out value="${line}" /><br/>
					</c:forEach>
				</div>
				</div>
				<div class="clearfix">
				<div class="float-right" style="width: 80%">
					<div class="">
						<c:forEach var="answerDto" items="${answerList }">
							<c:if test="${ questionDto.questionId == answerDto.questionId }">
								<div class="mt-2 card">
									<div class="card-header">
										<div class="my-auto" style="width: 50%; float: left">
											回答者：
											<c:out value="${ answerDto.answererName }" />
										</div>
										<div class="my-auto text-right"
											style="width: 50%; float: left">
											回答日時：
											<c:out value="${ answerDto.time }" />
										</div>
									</div>
									<div class="card-body">
										<c:forEach var="line" items="${fn:split(answerDto.answer, newLineChar)}" >
											<c:out value="${line}" /><br/>
										</c:forEach>
									</div>
								</div>
							</c:if>
						</c:forEach>
					</div>
				</div>
			</div>
			</c:forEach>
			<br>
		<c:if test="${ not empty user }">
		<p>質問投稿</p>
			<form action="add-question" method="post">
			<textarea maxlength="1000" name="question"class="form-control"></textarea>
			<input type="hidden" name="courseId" value="${ courseId }"><br>
			<button class="btn btn-outline-primary float-right">送信</button>
			</form>
		</c:if>
		<br><br><br>
		<p align="center">お問い合わせはこちら kelonos@gmail.com</p>
		<br>
	</div>
</body>
</html>