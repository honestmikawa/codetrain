<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>利用者追加</title>
<%@ include file="header.jsp"%>
<style>
.bg-col {
  background-color: #F5FFFA;
  color: #17a2b8;
  border-radius: 10px;
  padding: 16px;
  margin-top: 16px;
}
.title{
    background:#F5FFFA;
}

.bg-color {
	background:#F5FFFA;
}
.bg-light {
	background-color: #F5FFFA!important;
}
</style>
</head>
<body>
	<%@ include file="manager-operator-navbar.jsp"%>
	<p class="h1 bg-col text-info rounded">アカウント作成</p>
		<div class="text-center">
		<p class="text-danger">
			<c:if test="${ not empty errorMessage }">
				<c:out value="${ errorMessage }" />
			</c:if>
		</p>
	</div>
	
	<div class="text-center">
		<form id="form" action="add-user" method="post">
			<div class="form-group">
				<label class="text-info">メールアドレス</label>
				<div class="form-row">
					<div class="col-4"></div>
					<div class="col-4">
						<input type="email" name="email" class="form-control"><br>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="text-info">氏名</label>
				<div class="form-row">
					<div class="col-4"></div>
					<div class="col-4">
						<input type="text" name="name" class="form-control"><br>
						<input type="hidden" name="managerid" value="${ ManagerId }">
					</div>
				</div>
			</div>
			<div class="text-center">
				<button type="submit" class="btn btn-outline-primary mt-4">新規登録</button>
			</div>

		</form>
	</div>
</body>
</html>