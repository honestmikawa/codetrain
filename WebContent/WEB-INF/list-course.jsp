<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>コース一覧</title>
<%@ include file="header.jsp"%>
<style type="text/css">
	<!--
		h3 { color : #330066 !important;
			 background : linear-gradient(-135deg, #DCDCDC, #EAD9FF) !important; }
		.card-title{ color : #330066 !important;}
	-->
</style>
<style>
	@keyframes stamp {
		0% {opacity: 0%}
		100% {opacity: 100%}
	}
</style>
</head>
<body>
	<%@ include file="navbar.jsp"%>
	<% 
		int i = 0;
	%>
	<div class="container">
		<div class="row">
			<div class="mx-auto text-primary">
				&nbsp;
				<c:out value="${ message }" />
			</div>
		</div>
		<div>
			<c:forEach items="${ categoryList }" var="categoryDto">
			<div style="position: relative">
				<h3 class="h3 mt-3 p-3 rounded">
					<c:out value="${ categoryDto.categoryName }" />
				</h3>
				<hr>
				<c:forEach items="${ courseList }" var="courseDto" varStatus="status1">
					<c:choose>
						<c:when
							test="${ categoryDto.categoryId eq courseDto.categoryId && not empty user}">
							<div class="card mb-2">
								<div class="card-body pb-0">
									<div class="card-title">
										<a href="detail-course?courseId=${courseDto.courseId }"><font color="#330066">
										<c:out value="${ courseDto.courseName }" /></font></a>
									</div>
									<p><p><p>
										&nbsp;&nbsp;&nbsp;学習目安時間 : 
										<c:set var="time" value="${ courseDto.estimatedTime*60 }"/>
										<c:set var="min" value="${ time%60 }"/>
										<c:set var="hour" value="${ (time-min)/60 }"/>
										<fmt:formatNumber value="${ hour }" maxFractionDigits="0" />
										時間
										<c:if test="${ min != 0 }">
										<c:out value="${ min }"/>
										分
										</c:if>
									</p>
									
									<p>
										&nbsp;&nbsp;&nbsp;概要 : <c:out value="${ courseDto.overview }" />
									</p>
									<c:set var="q" value="1"/>
									<c:forEach items="${ studyHistoryList }" var="studyHistoryDto" varStatus="status2">
											<c:if
												test="${ studyHistoryDto.courseId eq courseDto.courseId }">
												<c:set value="${ studyHistoryDto.maxPage }" var="now" />
												<c:set value="${ courseDto.totalPages }" var="total" />
												<c:set value="${ (now/total)*100 }" var="train" />
												<c:set value="${ time-time*(now/total) }" var="rest"/>												
												<c:set var="min" value="${ rest%60 }"/>
												<c:set var="hour" value="${ (rest-min)/60 }"/>
												<style>
													@keyframes move<c:out value="${status1.index}"/> {
														0% {padding-left: 0%;}
														100% {padding-left: ${train-8}%}
													}
												</style>
												<div style="animation: move<c:out value='${status1.index }' /> 3s cubic-bezier(0.515, 0.895, 0.560, 0.970) 0s 1 normal; animation-fill-mode: forwards;">
												<p style="position: relative;">
												<c:choose>
												<c:when test="${ rest != 0 }">
												目的地まで残り
												<c:if test="${ hour>=1 }">
												<fmt:formatNumber value="${ hour }" maxFractionDigits="0" />
												時間
												</c:if>
												<c:if test="${ min != 0 }">
												<fmt:formatNumber value="${ min }" maxFractionDigits="0" />
												分
												</c:if>
												</c:when>
												<c:when test="${ rest == 0 }">
												<span style="position: absolute; top: -50px; left: 15px;">
												<img src="img/goal.png" height="100" width="100" style="position:relative; bottom: 50px; right: 10px; z-index: 3; opacity: 0%; animation: 1s stamp ease 3s 1 normal; animation-fill-mode: forwards;"/>
												</span>
												</c:when>
												</c:choose>
												</p>
													<img src="img/train.png" height="60" width="100" style="position:relative; z-index:2"/>
													<c:set var="q" value="2"/>
												</div>
											</c:if>
									</c:forEach>
									<c:if test="${ q == 1 }">
										<img src="img/train.png" height="60" width="100" />
									</c:if>
									<img src="img/station.png" height="60" width="100" style="position:absolute; right:0px; bottom:0px; z-index:1"/>
								</div>
							</div>
							<% 
		                       i++; 
	                         %>
						</c:when>
						<c:when
							test="${ categoryDto.categoryId eq courseDto.categoryId && courseDto.freeFlg eq true}">
							<div class="card mb-2">
								<div class="card-body">
									<div class="card-title">
										<a href="detail-course?courseId=${courseDto.courseId }"><c:out value="${ courseDto.courseName }" />
										</a>
									</div>
									<p>
										学習目安時間
										<c:set var="time" value="${ courseDto.estimatedTime*60 }"/>
										<c:set var="min" value="${ time%60 }"/>
										<c:set var="hour" value="${ (time-min)/60 }"/>
										<fmt:formatNumber value="${ hour }" maxFractionDigits="0" />
										時間
										<c:if test="${ min != 0 }">
										<c:out value="${ min }"/>
										分
										</c:if>
									</p>
									<p>
										<c:out value="${ courseDto.overview }" />
									</p>
								</div>
							</div>
							<% 
		                       i++; 
	                         %>
						</c:when>
						<c:when test="${ categoryDto.categoryId eq courseDto.categoryId }">
							<div class="card mb-2">
								<div class="card-body">
									<div class="card-title">
										<p>
											<c:out value="${ courseDto.courseName }" />
										</p>
									</div>
									<p>
										学習目安時間
										<c:set var="time" value="${ courseDto.estimatedTime*60 }"/>
										<c:set var="min" value="${ time%60 }"/>
										<c:set var="hour" value="${ (time-min)/60 }"/>
										<fmt:formatNumber value="${ hour }" maxFractionDigits="0" />
										時間
										<c:if test="${ min != 0 }">
										<c:out value="${ min }"/>
										分
										</c:if>
									</p>
									<p>
										<c:out value="${ courseDto.overview }" />
									</p>
								</div>
							</div>
							<% 
		                       i++; 
	                         %>
						</c:when>
					</c:choose>
				</c:forEach>
				<p style="position:absolute; top:25px; right:80px;">コース数<%= i %></p>
				<% i = 0; %>
				</div>
			</c:forEach>
		</div><br>
		<c:if test="${ not empty user }">
		<div style="height:50px;">
			<a href="edit-password">
				&nbsp;&nbsp;&nbsp;
				<button class="btn btn-primary float-right">パスワードを変更</button>
			</a>
		</div><br>
		</c:if>
	</div>
</body>
</html>