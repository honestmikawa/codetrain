<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
	<c:choose>
		<c:when test="${empty manager }">
			<a class="navbar-brand" href="list-manager">	
		</c:when>
		<c:otherwise>
			<a class="navbar-brand" href="list-user">
		</c:otherwise>
	</c:choose>
	<img src="../img/CodeTrain.png" height="30" width="41" /></a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<div class="mr-auto"></div>
		<div class="navbar-text small mr-3 text-secondary">
			<c:out value="${ not empty manager.name ? manager.name += 'さん、こんにちは' : operator.name += 'さん、こんにちは' }" />
		</div>
		<form action="../logout" method="post" class="form-inline">
			<button type="submit" class="btn btn-outline-danger btn-sm">ログアウト</button>
		</form>
	</div>
</nav>