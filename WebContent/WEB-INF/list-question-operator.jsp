<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<% pageContext.setAttribute("newLineChar", "\n"); %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>質問リスト</title>
<%@ include file="header.jsp"%>
<style>
.bg-light {
 background-color: #CCFF99!important;
}
</style>
<style>
.my-auto {
 background-color: #CCFF99!important;
}
</style>
<style>
.card-header {
 background-color: #CCFF99!important;
}
</style>
</head>
<body>
	<%@ include file="manager-operator-navbar.jsp"%>
	<!-- メッセージの色変更　by古瀬 -->
	<div class="text-center">
		<p class="mx-auto text-danger">
			<c:out value="${ message }" />
		</p>
		<div class="row">
			<div class="mx-auto text-primary">
				&nbsp;<c:out value="${ successMessage }" />
			</div>
		</div>
	</div>
	<div class="mx-auto clearfix" style="width: 70%;">
		<c:forEach var="questionDto" items="${questionList}"
			varStatus="status">
			<div class="mt-1 mb-2 card mx-auto">
				<div class="card-header">
					<div style="width: 30%; float: left;">
						<p class="my-auto h5 card-text">
							質問
							<c:out value="${status.index+1}" />
						</p>
					</div>
					<div class="my-auto text-right" style="width: 70%; float: left">
						質問日時：
						<c:out value="${questionDto.time }" />
					</div>
				</div>
				<div class="card-body">
					<c:forEach var="line" items="${fn:split(questionDto.question, newLineChar)}" >
						<c:out value="${line}" /><br/>
					</c:forEach>
				</div>
			</div>
			<div class="clearfix">
				<div class="float-right" style="width: 80%">
					<div class="">
						<c:forEach var="answerDto" items="${answerList }">
							<c:if test="${ questionDto.questionId == answerDto.questionId }">
								<div class="mt-2 card">
									<div class="card-header">
										<div class="my-auto" style="width: 50%; float: left">
											回答者：
											<c:out value="${ answerDto.answererName }" />
										</div>
										<div class="my-auto text-right"
											style="width: 50%; float: left">
											回答日時：
											<c:out value="${ answerDto.time }" />
										</div>
									</div>
									<div class="card-body">
										<c:forEach var="line" items="${fn:split(answerDto.answer, newLineChar)}" >
											<c:out value="${line}" /><br/>
										</c:forEach>
									</div>
								</div>
							</c:if>
						</c:forEach>
						<form action="add-answer" method="post" class="text-right mt-2">
							<textarea name="answer" class="form-control"></textarea>
							<input type="hidden" name="courseId" value="${ questionDto.courseId }">
							<input type="hidden" name="questionId" value="${ questionDto.questionId }">
							<button type="submit" class="btn btn-outline-success">送信</button>
						</form>
					</div>
				</div>
			</div>
		</c:forEach>
	</div>
</body>
</html>