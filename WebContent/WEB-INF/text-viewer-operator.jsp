<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>テキスト表示</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" />
	<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/pdfjs-dist@2.4.456/build/pdf.min.js"></script>
	<script type="text/javascript">
	<!--
		var currentPage = 1;
		var loadedPdf;
		var totalPages = 0;
		var url = "<c:out value='${serverUrl}'/>";

		function loadPdf(resolve, reject) {
			// Asynchronous download of PDF		
			var loadingTask = pdfjsLib.getDocument(url);
			loadingTask.promise.then(function(pdf) {
			  loadedPdf = pdf;
			  totalPages = loadedPdf.numPages;
			  resolve();
			}, function (reason) {
			  // PDF loading error
			  reject();
			});
		}
		
		function pageRendering(canvas, pageNumber) {
			loadedPdf.getPage(pageNumber).then(function(page) {
				var scale = 1.0;
			    var viewport = page.getViewport({scale: scale});
		
			    // Prepare canvas using PDF page dimensions
			    var context = canvas.getContext('2d');
			    var viewport = page.getViewport({ scale: scale, });
			    canvas.width = viewport.width;
			    canvas.height = viewport.height;
		
			    // Render PDF page into canvas context
			    var renderContext = {
			      canvasContext: context,
			      viewport: viewport
			    };
			    var renderTask = page.render(renderContext);
			    renderTask.promise.then(function() {
			      console.log('Page rendered');
			    });
			});
		}
		
		// ページが読み込まれた時
		$(function () {
			initializePage();
		});
		
		function initializePage() {
			// PDFを読み込む
			var loadTask = new Promise(function (resolve, reject) { 
					loadPdf(resolve, reject) 
				});
			// PDFを読み込んだらページをレンダリングする。
			loadTask.then(function (msg) {
			    pageRendering(document.getElementById('pdfCanvas'), currentPage);
			    setPageIndex();
			}).catch(function () {
				$("#text-area").remove();
				$("#message").append("<p class='text-danger text-center'>要求されたテキストは存在しません</p>");
			});			
		}
		
		// 前へボタン押下時
		function previousPage() {
			// 現在が最初のページだったら何もしない
			if (currentPage - 1 <= 0) {
				return;
			}
			// ページ数を戻す
			currentPage -= 1;
			
			// 更新後のページをレンダリングする
		    pageRendering(document.getElementById('pdfCanvas'), currentPage);
			
		    setPageIndex();
		}
		
		// 次へボタン押下時
		function nextPage() {
			// 現在が最終ページだったら何もしない
			if (currentPage + 1 > totalPages) {
				return;
			}
			// ページ数を進める。
			currentPage += 1;

			// 更新後のページをレンダリングする
		    pageRendering(document.getElementById('pdfCanvas'), currentPage);
		    
		    setPageIndex();
		}
		
		function jumpPage(jumpAt) {
			// 現在のページをジャンプ先のページに設定
			currentPage = jumpAt;
			
			// 更新後のページをレンダリングする
		    pageRendering(document.getElementById('pdfCanvas'), currentPage);
		    
		    setPageIndex();
		}
		
		function setPageIndex() {
			$("#pageIndex").empty();
			for(let i = -3; i < 4; i++) {
				if (currentPage + i <= 0 || currentPage + i > totalPages) {
					continue;
				}
				let linkNum = currentPage + i;
				if (i != 0) {
					var element = "<a class='pageIndex' href='javascript:jumpPage(" + linkNum + ")'>" + linkNum + "</a>";						
				} else {
					var element = "<span class='pageIndex'>" + linkNum + "</span>";	
				}
				$("#pageIndex").append(element);	
			}
		}
		//-->
	</script>
	<style>
		.pageIndex {
			margin-left:2px;
			margin-right:2px;
		}
	</style>
</head>
<body>
	<form class="text-right" action="detail-course">
		<input type="hidden" name="courseId" value=<c:out value="${courseId }"/>>
		<button class="btn btn-success mr-5 mt-5" style="position:absolute; top: 5%; right: 5%;">終了</button>
	</form>
	<div id="message"></div>
	<div id="text-area">
		<div class="text-center">
			<canvas class="mt-2 mx-auto" id="pdfCanvas"></canvas>
		</div>
		<div class="text-center">
			<button class="btn btn-outline-success" type="button" onclick="previousPage()">前へ</button>
			<span id="pageIndex"></span>
			<button class="btn btn-outline-success" type="button" onclick="nextPage()">次へ</button>
		</div>
	</div>
</body>
</html>