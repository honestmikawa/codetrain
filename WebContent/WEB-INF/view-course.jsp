<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><c:out value="${ not empty isAdd ? 'コースの追加' : 'コースの編集'}" /></title>
<%@ include file="header.jsp"%>
<script type="text/javascript">
<!--
	$(function() {
		// 削除ボタン
		$(document).on('click', '#delete', function() {
			if (!confirm("本当に削除しますか？")) {
				return false;
			}
			// 送信先URLを設定
			$('#form').attr('action', 'delete-course');
			// 送信
			$('#form').submit();
		});
	});
// -->
<!--
	$(function() {
		// 続けて追加ボタン
		$(document).on('click', '#continueAdd', function() {
			$('#continueAdd').append('<input type="hidden" name="continueAdd" value="continueAdd" />');
		});
	});
// -->
</script>
<style>
.bg-light {
 background-color: #CCFF99!important;
}
</style>
<style>
thead {
 background-color: #CCFF99!important;
}
</style>
<style>.table-hover tbody tr:hover td, .table-hover tbody tr:hover th 
{
  background-color: #CCFF99;
}
</style>
<style>
option {
 background-color: #CCFF99!important;
}
</style>
</head>

<body>
	<%@ include file="manager-operator-navbar.jsp"%>
		<div class="container">
			<!-- メッセージorエラーメッセージ-->
			<c:forEach items="${ errorMessageList }" var="errorMessage">
				<div class="row">
					<div class="mx-auto text-danger">
						&nbsp;<c:out value="${ errorMessage }" />
					</div>
				</div>
			</c:forEach>
				<div class="row">
					<div class="mx-auto text-primary">
						&nbsp;<c:out value="${ message }" />
					</div>
				</div>
			<!-- タイトル -->
			<p class="h3 mt-3 p-3 bg-light text-success rounded">
				<c:out value="${ not empty isAdd ? 'コースの追加' : 'コースの編集'}" />
			</p>
				<form id="form" action="${ not empty isAdd ? 'add-course' : 'edit-course' }" method="post" enctype="multipart/form-data">
					<div class="form-group row">
						<!-- コースID -->
						<input type="hidden" name="courseId" value="${ courseDto.courseId }" />
					</div>
					<div class="form-row">
						<!-- コース追加画面におけるカテゴリ名のセレクトボックス -->
						<c:if test="${ not empty isAdd }">
							<div class="form-group col-md-6">
								<label for="categoryId"><font color="green">カテゴリ名</font></label>
					<select class="form-control" id="categoryId" name="categoryId">
							<c:forEach items="${ list }" var="categoryDto">
								<c:choose>
									<c:when test="${ not empty newCategory }">
										<c:choose>
											<c:when test="${ categoryDto.categoryName eq newCategory }">
												<option value="${ categoryDto.categoryId }" selected>
												<c:out value="${ categoryDto.categoryName }" />
												</option>
											</c:when>
											<c:when test="${ categoryDto.categoryName != newCategory }">
												<option value="${ categoryDto.categoryId }">
												<c:out value="${ categoryDto.categoryName }" />
												</option>
											</c:when>
										</c:choose>
									</c:when>
									<c:otherwise>
										<option value="${ categoryDto.categoryId }">
										<c:out value="${ categoryDto.categoryName }" />
										</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</select> 
						<a href="add-category?isAdd=true" ><font color="green">カテゴリを追加する</font></a>
					  		</div>
				  		</c:if>
				  		<!-- コース編集画面におけるカテゴリ名のセレクトボックス -->
				  		<c:if test="${ empty isAdd }">
					  		<div class="form-group col-md-6">
								<label for="categoryId"><font color="green">カテゴリ名</font></label>
							    <select class="form-control" id="categoryId" name="categoryId">
								    <c:forEach items="${ list }" var="categoryDto">
								<c:choose>
									<c:when test="${ not empty newCategory }">
										<c:choose>
											<c:when test="${ categoryDto.categoryName eq newCategory }">
												<option value="${ categoryDto.categoryId }" selected>
													<c:out value="${ categoryDto.categoryName }" />
												</option>
											</c:when>
											<c:when test="${ categoryDto.categoryName != newCategory }">
												<option value="${ categoryDto.categoryId }">
													<c:out value="${ categoryDto.categoryName }" />
												</option>
											</c:when>
										</c:choose>
									</c:when>
									<c:otherwise>
										<c:choose>
											<c:when test="${ categoryDto.categoryName != courseDto.categoryName }">
												<option value="${ categoryDto.categoryId }">
												<c:out value="${ categoryDto.categoryName }" />
												</option>
											</c:when>
											<c:otherwise>
												<option value="${ courseDto.categoryId }" selected>
												<c:out value="${ courseDto.categoryName }"/>
												</option>
											</c:otherwise>
										</c:choose>
									</c:otherwise>
								</c:choose>
							</c:forEach>
					    		</select>
					    		<a href="add-category?courseId=<c:out value='${ courseDto.courseId }'/>"><font color="green">カテゴリを追加する</font></a>
					  		</div>
				  		</c:if>
				  		<!-- 余白 -->
						<div class="form-group col-md-1">
							<h6> </h6>
						</div>
				  		<!-- コース追加画面におけるフリーコースのチェックボックス -->
				  		<c:if test="${ not empty isAdd }">
					  		<div class="form-group　form-check　col-md-3">
		    					<input type="checkbox" class="form-check-input" id="freeFlg" name="freeFlg" >
		    					<label class="form-check-label" for="freeFlg"><font color="green">フリーコース</font></label>
		 					</div>
	 					</c:if>
	 					<!-- コース更新画面におけるフリーコースのチェックボックス -->
				  		<c:if test="${ empty isAdd }">
					  		<div class="form-group　form-check　col-md-3">
					  			<c:choose>
						  			<c:when test="${ courseDto.freeFlg eq true }">
				    					<input type="checkbox" class="form-check-input" id="freeFlg" name="freeFlg" checked>
				    					<label class="form-check-label" for="freeFlg"><font color="green">フリーコース</font></label>
			    					</c:when>
			    					<c:otherwise>
			    						<input type="checkbox" class="form-check-input" id="freeFlg" name="freeFlg">
				    					<label class="form-check-label" for="freeFlg"><font color="green">フリーコース</font></label>
			    					</c:otherwise>
		    					</c:choose>
		 					</div>
	 					</c:if>
	 				</div>
	 				<div class="form-row">	
		 				<!-- コース名のテキストボックス -->
	 					<div class="form-group col-md-6">
						    <label for="courseName"><font color="green">コース名</font></label>
						    <input type="text" class="form-control" id="courseName" name="courseName" value="${ not empty isAdd ? '' : courseDto.courseName }" placeholder="例）Java応用">
						</div> 		
	 					<!-- 目標学習時間のテキストボックス -->
	 					<div class="form-group col-md-6">
						    <label for="estimatedTime"><font color="green">目標学習時間</font></label>
						    <input type="number" class="form-control" id="estimatedTime" name="estimatedTime" min="1" max="1000" value="${ not empty isAdd ? '1' : courseDto.estimatedTime }" placeholder="例）100">
						</div>
					</div>			
					<!-- 前提条件のテキストエリア -->
					<div class="form-group">
					    <label for="precondition"><font color="green">前提条件</font></label>
					    <pre><textarea class="form-control" id="precondition" name="precondition" placeholder="例）Java基礎を学習済みであること" rows="3"><c:out value="${ not empty isAdd ? '' : courseDto.precondition }"></c:out></textarea></pre>
					</div>
					<!-- 概要のテキストエリア -->
					<div class="form-group">
					    <label for="overview"><font color="green">概要</font></label>
					    <pre><textarea class="form-control" id="overview" name="overview" placeholder="例）Javaの応用について学習するコースです。" rows="3"><c:out value="${ not empty isAdd ? '' : courseDto.overview }"></c:out></textarea></pre>
					</div>
					<!-- ゴールのテキストエリア -->
					<div class="form-group">
					    <label for="goal"><font color="green">ゴール</font></label>
					    <pre><textarea class="form-control" id="goal" name="goal" placeholder="例）Javaの基礎から応用まで身につけること" rows="3"><c:out value="${ not empty isAdd ? '' : courseDto.goal }" ></c:out></textarea></pre>
					</div>
					<!-- 目次のテキストボックス -->
					<div class="form-group">
					    <label for="contents"><font color="green">目次</font></label>
					    <pre><textarea class="form-control" id="contents" name="contents" placeholder="例）Javaとは&#13;&#10;例）オブジェクト指向プログラミング&#13;&#10;例）API" rows="3"><c:out value="${ not empty isAdd ? '' : courseDto.contents }"></c:out></textarea></pre>
					</div>
					<!-- コース追加画面におけるPDF,送信ボタン -->
					<c:if test="${ not empty isAdd }">
						<div class="row">
							<!-- PDF -->
							<div class="input-group col-md-5">
							    <div class="custom-file">
								    <input type="file" class="custom-file-input" id="pdf" name="pdf" accept="application/pdf">>
								    <label class="custom-file-label" for="inputFile" data-browse="参照">ファイルを選択してください</label>
							    </div>	
							</div>
							<!-- 余白 -->
							<div class="form-group col-md-3">
								<h6> </h6>
							</div>
							<!-- 追加ボタン -->
							<div class="form-group col-md-1">
								<button type="submit" class="btn btn-outline-success">追加</button>
							</div>
							<!-- 余白 -->
							<div class="form-group col-md-0.5">
								<h6> </h6>
							</div>
							<!-- 続けて追加ボタン -->
							<div class="form-group col-md-1">
								<button type="submit" class="btn btn-outline-success" id="continueAdd">続けて追加</button>
							</div>
						</div>	
					</c:if>
					<!-- コース更新画面におけるPDF,送信ボタン -->
					<c:if test="${ empty isAdd }">
						<div class="row">
							<!-- PDF -->
							<div class="d-inline col-md-5">
							    <div class="custom-file">
								    <input type="file" class="custom-file-input" id="pdf" name="pdf" accept="application/pdf">
								    <label class="custom-file-label" for="inputFile" data-browse="参照">ファイルを選択してください</label>
							    </div>	
							</div>
							<!-- 余白 -->
							<div class="d-inline col-md-3">
								<h6> </h6>
							</div>
							<!-- 更新ボタン -->
							<div class="d-inline col-md-1">
								<button type="submit" class="btn btn-outline-success">更新</button>
							</div>
							<!-- 余白 -->
							<div class="d-inline col-md-0.5">
								<h6> </h6>
							</div>
					</c:if>	
			  	</form>
			  	<c:if test="${ empty isAdd }">  		
			  		<form id="delete" action="delete-course" method="post" enctype="multipart/form-data">
			  			<!-- 削除ボタン -->
						<div class="class="d-inline col-md-1">
							<button type="submit" class="btn btn-danger">削除</button>
							<input type="hidden" name="courseId" value="${ courseDto.courseId }" />
						</div>					
					</form>
				</c:if>
						</div>
					</div>		
		<br><br>
		<script src="https://cdn.jsdelivr.net/npm/bs-custom-file-input/dist/bs-custom-file-input.js"></script>
		<script>
  			bsCustomFileInput.init();
		</script>
</body>
</html>