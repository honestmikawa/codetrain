<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
	pageContext.setAttribute("newLineChar", "\n");
%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="header.jsp"%>
<meta charset="UTF-8">
<title>コース詳細</title>
<style type="text/css">
	<!--
		h1 { color : #330066 !important;
			 background : linear-gradient(-135deg, #DCDCDC, #EAD9FF) !important; }
		.card-header { color : #330066 !important; 
					   background : linear-gradient(-135deg, #DCDCDC, #EAD9FF) !important; 
					   padding : 0.1px 0.1px 0.1px !important; }
	-->
</style>
</head>
<body>
	<%@ include file="navbar.jsp"%>
	<div class="container">
		<h1 class="mt-3 p-3 bg-light rounded">
			<c:out value="${courseInformation.courseName}" />
		</h1>
		<br>
		<table>
			<tr>
				<td>
					<form action="list-course" method="get">
						<button type="submit" class="btn btn-outline-primary ">コース一覧</button>
					</form>
				</td>
				<td>
					<form action="list-question" method="get">
						<input type="hidden" name="courseId"
							value="${ courseInformation.courseId }" />
						<button type="submit" class="btn btn-outline-primary ">質問掲示板</button>
					</form>
				</td>
				<td>
					<form action="text-viewer" method="get">
						<input type="hidden" name="courseId"
							value="${ courseInformation.courseId }">
						<button type="submit" class="btn btn-outline-primary ">最初から</button>
					</form>
				</td>
				<td><c:if test="${not empty user }">
						<form action="text-viewer" method="get">
							<input type="hidden" name="courseId"
								value="${ courseInformation.courseId }"> <input
								type="hidden" name="isContinue" value="true">
							<button type="submit" class="btn btn-outline-primary ">続きから</button>
						</form>
					</c:if></td>
			</tr>
		</table>
		<br>
		<div class="card mb-2" style="width: 1110px;">
			<div class="card-body">
				<font color="#330066"><b>カテゴリ：</b></font>
				<c:out value="${courseInformation.categoryName}" />
				&emsp; 
				<font color="#330066"><b>コース：</b></font>
				<c:out value="${courseInformation.courseName}" />
				&emsp;
				<font color="#330066"><b>学習時間目安：</b></font>
				<c:out value="${courseInformation.estimatedTime}" />
				時間&emsp;
				<font color="#330066"><b>前提条件：</b></font>
				<c:out value="${courseInformation.precondition}" />
				<br> <br>
				<div class="card mb-2" style="width: 100%;">
					<div class="card-header" >
						<p class="h3 mt-3 p-3 my-auto rounded">概要</p>
					</div>
					<div class="card-body">
						<c:forEach var="line"
							items="${fn:split(courseInformation.overview, newLineChar)}">
							<c:out value="${line}" />
							<br />
						</c:forEach>
					</div>
				</div>
				<div class="card mb-2" style="width: 100%;">
					<div class="card-header" >
						<p class="h3 mt-3 p-3 my-auto rounded">ゴール</p>
					</div>
					<div class="card-body">
						<c:forEach var="line"
							items="${fn:split(courseInformation.goal, newLineChar)}">
							<c:out value="${line}" />
							<br />
						</c:forEach>
					</div>
				</div>
				<div class="card mb-2" style="width: 100%;">
					<div class="card-header">
						<p class="h3 mt-3 p-3 my-auto rounded">目次</p>
					</div>
					<div class="card-body">
						<c:forEach var="line"
							items="${fn:split(courseInformation.contents, newLineChar)}">
							<c:out value="${line}" />
							<br />
						</c:forEach>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>