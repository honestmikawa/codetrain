<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="header.jsp"%>
<meta charset="UTF-8">
<title>請求明細確認</title>
</head>
<body topmargin="500">
<br><br><br>
	<div class="container">
		<div align="center">
			<font size="6"><u>請求明細書</u></font>
		</div>
		<br>
		<c:forEach items="${billingList}" var="billing">
			<c:forEach items="${priceList}" var="price">
				<u><input type="hidden" name="time" value="${ billing.time }">
					<font size="3"><fmt:parseNumber
							value="${ billing.time / 100}" integerOnly="true" /> 年<c:out
							value="${billing.time % 100}" />月分</font></u>
				<br>
				<br>
				<c:out value="${ billing.billingAddress }"></c:out>
				<br>
				<font size="5"><u><c:out value="${billing.name}" />様</u></font>
				<br>
				<br>
				<font size="2">下記の通りご請求申し上げます。</font>
				<br>
				<font size="5"><u>合計金額&emsp;&yen;<fmt:formatNumber
							value="${((billing.userNumber - billing.pauserNumber) * price.price) + (billing.pauserNumber * (price.price / 2))}" />-
				</u></font>
				<br>
				<table class="table table-bordered">
					<thead class="thead-light" align="center">
						<tr>
							<th class="text-secondary">内訳</th>
							<th class="text-secondary">人数</th>
							<th class="text-secondary">単価</th>
							<th class="text-secondary">金額</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>利用者数</td>
							<td align="right"><c:out
									value="${billing.userNumber - billing.pauserNumber }" /></td>
							<td align="right"><fmt:formatNumber value="${price.price }" /></td>
							<td align="right"><fmt:formatNumber
									value="${(billing.userNumber - billing.pauserNumber) * price.price }" />
							</td>
						</tr>
						<tr>
							<td>休止者</td>
							<td align="right"><c:out value="${billing.pauserNumber}"></c:out></td>
							<td align="right"><fmt:formatNumber
									value="${ price.price / 2}" /></td>
							<td align="right"><fmt:formatNumber
									value="${billing.pauserNumber * (price.price / 2)}" /></td>
						</tr>
						<tr>
							<td>総利用者</td>
							<td align="right"><c:out value="${billing.userNumber}" /></td>
							<td></td>
							<td align="right"><fmt:formatNumber
									value="${((billing.userNumber - billing.pauserNumber) * price.price) + (billing.pauserNumber * (price.price / 2))}" />
						</tr>
					</tbody>
				</table>
			</c:forEach>
		</c:forEach>
	</div>
</body>
</html>