<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>運用者一覧</title>
<%@ include file="header.jsp"%>
<style>
.bg-light {
 background-color: #CCFF99!important;
}
</style>
<style>
thead {
 background-color: #CCFF99!important;
}
</style>
<style>.table-hover tbody tr:hover td, .table-hover tbody tr:hover th 
{
  background-color: #CCFF99;
}
</style>
</head>
<body>
<%@ include file="manager-operator-navbar.jsp"%>
	<div class="container">
	<p class="h3 mt-3 p-3 bg-light text-success rounded">運営担当者一覧</p>
		<c:choose>
			<c:when test="${ operator.authority eq 'HIGH' }">
				<!-- エラーメッセージ　by古瀬 -->
				<c:forEach items="${ errorMessageList }" var="errorMessage">
					<div class="row">
						<div class="mx-auto text-danger">
							&nbsp;<c:out value="${ errorMessage }" />
						</div>
					</div>
				</c:forEach>
					<div class="row">
						<div class="mx-auto text-primary">
							&nbsp;
							<c:out value="${ message }" />
						</div>
					</div>
				<table class="table table-hover">
					<thead  class="text-secondary text-center">
						<th><font color="green">id</font></th>
						<th><font color="green">氏名</font></th>
						<th><font color="green">権限</font></th>
						<th>  </th>
					</thead>
					<c:forEach items="${ operatorList }" var="operatorDto">
					<form action="edit-operator" method="post" enctype="multipart/form-data">
						<tbody class="text-center">
							<td>
							<c:out value="${ operatorDto.operatorId }" />
							<!-- 運営者ID追加　by古瀬 -->
							<input type="hidden" name="operatorId" value=<c:out value="${ operatorDto.operatorId }"/> /></td>
							<td>
							<input type="text" maxlength="1000" class="form-control" name="operatorName"
										value=<c:out value="${ operatorDto.name }"/>> 
							</td>
							<td>
							<select name="authority" class="form-control">
								<!-- ログインユーザー自身の権限は変更できない処理　by古瀬 -->
								<c:if test="${ sessionScope.operator.operatorId != operatorDto.operatorId }">
									<c:choose>
										<c:when test="${operatorDto.authority eq 'LOW'}">
											<option selected><c:out value="${operatorDto.authority}" /></option>
											<option>MIDDLE</option>
											<option>HIGH</option>
										</c:when>
										<c:when test="${operatorDto.authority eq 'MIDDLE' }">
											<option>LOW</option>
											<option selected><c:out value="${ operatorDto.authority}" /></option>
											<option>HIGH</option>
										</c:when>
										<c:when test="${operatorDto.authority eq 'HIGH'}">
											<option>LOW</option>
											<option>MIDDLE</option>
											<option selected><c:out value="${operatorDto.authority}" /></option>
										</c:when>
									</c:choose>
								</c:if>
								<c:if test="${ sessionScope.operator.operatorId == operatorDto.operatorId }">
									<option selected><c:out value="${operatorDto.authority}" /></option>
								</c:if>
							</select>
							</td>
							<td>
							<button type="submit" class="btn btn-outline-success">保存</button>
							</td>
						</tbody>	
					</form>
					</c:forEach>	
				</table>
			</c:when>
			<c:otherwise>
				<p>アクセス権限がありません</p>
			</c:otherwise>
		</c:choose>
	</div>
</body>
</html>