<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>利用者一覧</title>
<%@ include file="header.jsp"%>
<script type="text/javascript">
	function Change(selector) {
		if (document.getElementById(selector).value == "") {
			alert("休止理由を入力してください")
			return false;
		} else {
			return true;
		}
	}

	function Delete() {
		return confirm("本当に削除しますか？")
	}
</script>
<style>
.bg-col {
  background-color: #F5FFFA;
  color: #17a2b8;
  border-radius: 10px;
  padding: 16px;
  margin-top: 16px;
}
.title{
    background:#F5FFFA;
}
 .table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
    background-color: #F5FFFA;
}
.bg-color {
	background:#F5FFFA;
}
.bg-light {
	background-color: #F5FFFA!important;
}
</style>
</head>
<body>
	<%@ include file="manager-operator-navbar.jsp"%>
	<h1 class="bg-col">利用者一覧</h1>
	<div class="container">
	
		<div class="text-right">
		<form id="form" action="/CodeTrain/manager/billing" method="get">
			<button type="submit" class="btn btn-link">請求明細</button>
		</form>
	</div>

	
	<div class="text-center">
		<p class="text-info">
			<c:if test="${ not empty Message }">
				<c:out value="${ Message }" />
			</c:if>
		</p>
		<p class="text-danger">
			<c:if test="${ not empty errorMessage }">
				<c:out value="${ errorMessage }" />
			</c:if>
		</p>

	</div>
	<div class="form-group row">
		<table class="table table-hover">
			<tr class="text-secondary text-center title">
			<th ><br></th>
				<th>名簿番号</th>
				<th>氏名</th>
				<th>利用者ID</th>
				<th>メールアドレス</th>
				<th>状態</th>
				<th>休止理由</th>

				<th><br></th>
				<th><br></th>
				<th><br></th>

			</tr>
			<c:forEach items="${ userList }" var="userDto" varStatus="status">

				<tr class="text-center">
				<td></td>
					<td class="align-middle"><c:out value="${ status.index + 1 }" /></td>
					<td class="align-middle"><a
						href="/CodeTrain/manager/detail-user?userId=${ userDto.userId }"><c:out
								value="${ userDto.name }" /></a></td>
					<td class="align-middle"><fmt:formatNumber value="${ userDto.userId }" pattern="0000" /></td>
					<td class="align-middle"><c:out value="${ userDto.email }" /></td>
					<td class="align-middle">
							<c:if test="${ userDto.status == 'active'}">
								活性
							</c:if>
							<c:if test="${ userDto.status == 'rest'}">
								非活性
							</c:if>
						 </td>
					<td>
						<form id="form<c:out value="${status.index}"/>"
							action="/CodeTrain/manager/edit-user" method="post">
							<input type="hidden" name="userId" value="${ userDto.userId }">
							<input type="hidden" name="status" value="${ userDto.status }">
							<input type="text" name="pauseReason"
								id="pauseReason<c:out value="${status.index }"/>"
								value="${ userDto.pauseReason }" class="form-control" />
						</form>
					</td>
					<td><c:choose>
							<c:when test="${ userDto.status == 'active'}">
								<td>
									<button type="submit" class="btn btn-warning"
										onclick="return Change('pauseReason<c:out value="${status.index}"/>')"
										form="form<c:out value="${status.index}"/>">休止</button>
								</td>
							</c:when>
							<c:otherwise>
								<td>
									<button type="submit" class="btn btn-success"
										form="form<c:out value="${status.index}"/>">復帰</button>
								</td>
							</c:otherwise>
						</c:choose>
					<td>
						<form action="/CodeTrain/manager/delete-user"
							method="post">
							<input type="hidden" name="userId" value="${ userDto.userId }">
							<button type="submit" class="btn btn-danger"
								onclick="return Delete()">削除</button>
						</form>
					</td>
				</tr>

			</c:forEach>

		</table>

	</div>

	<div class="text-right">
		<form id="form" action="/CodeTrain/manager/add-user" method="get">
			<button type="submit" class="btn btn-outline-primary mt-4">アカウント作成</button>
		</form>
		<a href="edit-password">
			<button class="mt-2 btn btn-primary">パスワードを変更</button>
		</a>
	</div>
	</div>
</body>
</html>