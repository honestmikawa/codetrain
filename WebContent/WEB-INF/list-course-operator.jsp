<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>運用者用コース一覧</title>
<%@ include file="header.jsp"%>
<style>
.bg-light {
 background-color: #CCFF99!important;
}
</style>
</head>
<body>
	<%@ include file="manager-operator-navbar.jsp"%>
	<% 
		int i = 0; 
	%>
	<div class="container">
		<div class="row">
			<div class="mx-auto text-success">
				&nbsp;
				<c:out value="${ message }" />
			</div>
		</div>
		<a href="add-course"><font color="green">コースを追加する</font></a>
		<div>
			<c:forEach items="${ categoryList }" var="categoryDto">
			<div style="position: relative">
				<p class="h3 mt-3 p-3 bg-light text-success rounded">
					<c:out value="${ categoryDto.categoryName }" />
				</p>
				<hr>
				<c:forEach items="${ courseList }" var="courseDto">
						<c:if
							test="${categoryDto.categoryId == courseDto.categoryId}">
							<div class="card mb-2">
								<div class="card-body">
									<div class="card-title">
										<a href="detail-course?courseId=${courseDto.courseId }"><font color="green"><c:out
												value="${ courseDto.courseName }" /></font></a>
									</div>
									<p>
										学習目安時間
										<c:set var="time" value="${ courseDto.estimatedTime*60 }"/>
										<c:set var="min" value="${ time%60 }"/>
										<c:set var="hour" value="${ (time-min)/60 }"/>
										<fmt:formatNumber value="${ hour }" maxFractionDigits="0" />
										時間
										<c:if test="${ min != 0 }">
										<c:out value="${ min }"/>
										分
										</c:if>
									</p>
									<p>
										<c:out value="${ courseDto.overview }" />
									</p>
								</div>
							</div>
							<% 
		                       i++; 
	                         %>
						</c:if>
				</c:forEach>
				<p style="position:absolute; top:25px; right:80px;">コース数<%= i %></p>
				<% i = 0; %>
				</div>
			</c:forEach>
		</div>
	</div>
</body>
</html>