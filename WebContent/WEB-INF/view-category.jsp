<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>カテゴリ追加</title>
<%@ include file="header.jsp"%>
<style>
.bg-light {
 background-color: #CCFF99!important;
}
</style>
<style>
thead {
 background-color: #CCFF99!important;
}
</style>
<style>.table-hover tbody tr:hover td, .table-hover tbody tr:hover th 
{
  background-color: #CCFF99;
}
</style>
</head>
<body>
	<%@ include file="manager-operator-navbar.jsp"%>
			<div class="container">
				<!-- メッセージorエラーメッセージ-->
				<c:forEach items="${ errorMessageList }" var="errorMessage">
					<div class="row">
						<div class="mx-auto text-danger">
							&nbsp;<c:out value="${ errorMessage }" />
						</div>
					</div>
				</c:forEach>
					<div class="row">
						<div class="mx-auto text-primary">
							&nbsp;<c:out value="${ message }" />
						</div>
					</div>
				<!-- タイトル -->
				<p class="h3 mt-3 p-3 bg-light text-success rounded">
					<c:out value="カテゴリの追加" />
				</p>
				<br>
				<br>
					<form id="form" action="add-category" method="post">
							<!-- カテゴリ名のテキストボックス -->
							<div class="center-block form-group col-md-12">
								<div class="form-group">
									   <input type="text" class="form-control" id="categoryName" name="categoryName" placeholder="例）Java">
								</div>
							</div>
							<br>
							<!-- 追加ボタン -->
							<div class="mx-auto" style="width:120px;">
							<div class="form-group">
								<button type="submit" class="btn btn-outline-success">追加</button>
							</div>
							</div>
					</form>
			</div>
			<br>	
</body>
</html>