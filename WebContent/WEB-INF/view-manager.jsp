<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>法人追加</title>
<%@ include file="header.jsp"%>
<style>
.bg-light {
 background-color: #CCFF99!important;
}
</style>
<style>
thead {
 background-color: #CCFF99!important;
}
</style>
<style>.table-hover tbody tr:hover td, .table-hover tbody tr:hover th 
{
  background-color: #CCFF99;
}
</style>
</head>
<body>
	<%@ include file="manager-operator-navbar.jsp"%>
	<p class="h1 mt-3 p-3 bg-light text-success rounded">法人アカウント作成</p>
	<div class="container">
		<div class="text-center">
			<p class="text-danger">
				<c:if test="${ not empty errorMessage }">
					<c:out value="${ errorMessage }" />
				</c:if>
			</p>
		</div>
				<c:choose>
			<c:when test="${ operator.authority == 'LOW' }">
		<p>アクセス権限がありません</p>
		</c:when>
		<c:otherwise>
		<form id="form" action="add-manager" method="post">
			<div class="form-group row">
				<label  class="col-sm-4 text-md-center"><font color="green">メールアドレス</font></label>
				<div class="col-sm-6"> 
					<input type="email" name="email" class="form-control"><br>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-4 text-md-center"><font color="green">役職</font></label>
				<div class="col-sm-6"> 
					<input type="text" class="form-control" name="position"><br>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-4 text-md-center"><font color="green">氏名</font></label>
				<div class="col-sm-6"> 
					<input type="text" name="name" class="form-control"><br>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-4 text-md-center"><font color="green">請求先</font></label>
				<div class="col-sm-6"> 
					<input type="text" name="billing_address" class="form-control"><br>
					</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-4 text-md-center"><font color="green">ドメイン</font></label>
				<div class="col-sm-6"> 
					<input type="text" name="domain" class="form-control"><br>
					</div>
			</div>
			<div class="text-center">
			<button type="submit" class="btn btn-outline-success mt-4">追加</button>
			</div>
		</form>		</c:otherwise>
		</c:choose>
	</div>
</body>
</html>