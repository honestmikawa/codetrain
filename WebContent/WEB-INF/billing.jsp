<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="header.jsp"%>
<meta charset="UTF-8">
<title>請求明細</title>
</head>
<body>
	<%@ include file="manager-operator-navbar.jsp"%>
	<div class="container">
		<h1>
			<b><p class="h4 mt-3 p-3 bg-light text-info rounded">請求明細</p> </b>
		</h1>
		<form action="list-user" method="get">
			<button type="submit" class="btn btn-primary ">一覧に戻る</button>
		</form>
		<table class="table table-hover table-bordered">
			<thead class="thead-light" align="center">
				<div class="form-group row">
					<tr class="text-cursive">
						<th class="text-secondary">年/月</th>
						<th class="text-secondary">金額</th>
						<th class="text-secondary">総利用人数</th>
						<th class="text-secondary">内休止者数</th>
						<th class="text-secondary">明細発行</th>
					</tr>
				</div>
			</thead>
			<tbody>
				<c:forEach items="${billingList}" var="billing">
					<tr>
						<td align="center"><fmt:parseNumber
								value="${ billing.time / 100}" integerOnly="true" /> 年<c:out
								value="${billing.time % 100}" />月</td>
						<td align="right"><c:out value="${ billing.billingAmount }" />円</td>
						<td align="right"><c:out value="${ billing.userNumber }" /></td>
						<td align="right"><c:out value="${ billing.pauserNumber }" /></td>
						<td align="center">
							<form action="billing-statement" method="get">
								<input type="hidden" name="time" value="${billing.time }">
								<button type="submit" class="btn btn-success">明細</button>
							</form>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</body>
</html>