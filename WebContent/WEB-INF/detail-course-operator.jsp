<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
	pageContext.setAttribute("newLineChar", "\n");
%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="header.jsp"%>
<style>
.bg-light {
 background-color: #CCFF99!important;
}
</style>
<style>
.card-header {
 background-color: #CCFF99!important;
 padding : 0.1px 0.1px 0.1px !important; 
}
</style>
<style>
p {
  background-color: #CCFF99;
}
</style>
<meta charset="UTF-8">
<title>運用者用コース詳細</title>
</head>
<body>
	<%@ include file="manager-operator-navbar.jsp"%>
	<div class="container">
		<p class="h1 mt-3 p-3 bg-light text-success rounded">
			<c:out value="${courseInformation.courseName}" />
		</p>
		<br>
		<table>
			<tr>
				<td>
					<form action="list-course-operator" method="get">
						<button type="submit" class="btn btn-outline-success ">コース一覧</button>
					</form>
				</td>
				<td>
					<form action="list-question" method="get">
						<input type="hidden" name="courseId"
							value="${ courseInformation.courseId }" />
						<button type="submit" class="btn btn-outline-success">質問掲示板</button>
					</form>
				</td>
				<td>
					<form action="text-viewer" method="get">
						<input type="hidden" name="courseId"
							value="${ courseInformation.courseId }">
						<button type="submit" class="btn btn-outline-success">テキスト</button>
					</form>
				</td>
				<td>
					<form action="edit-course" method="get">
						<input type="hidden" name="courseId"
							value="${ courseInformation.courseId }">
						<button type="submit" class="btn btn-outline-success ">編集</button>
					</form>
				</td>
			</tr>

		</table>
		<br>
		<div class="card mb-2" style="width: 1110px;">
			<div class="card-body">
				<font color="green"><b>カテゴリ：</b></font>
				<c:out value="${courseInformation.categoryName}" />
				&emsp; <font color="green"><b>コース：</b></font>
				<c:out value="${courseInformation.courseName}" />
				&emsp; <font color="green"><b>学習時間目安：</b></font>
				<c:out value="${courseInformation.estimatedTime}" />
				時間&emsp; <font color="green"><b>前提条件：</b></font>
				<c:out value="${courseInformation.precondition}" />
				<br> <br>
				<div class="card mb-2" style="width: 100%;">
					<div class="card-header">
						<p class="h3 mt-3 p-3 my-auto bg-light text-success rounded">概要</p>
					</div>
					<div class="card-body">
						<c:forEach var="line"
							items="${fn:split(courseInformation.overview, newLineChar)}">
							<c:out value="${line}" />
							<br />
						</c:forEach>
					</div>
				</div>
				<div class="card mb-2" style="width: 100%;">
					<div class="card-header">
						<p class="h3 mt-3 p-3 my-auto bg-light text-success rounded">ゴール</p>
					</div>
					<div class="card-body">
						<c:forEach var="line"
							items="${fn:split(courseInformation.goal, newLineChar)}">
							<c:out value="${line}" />
							<br />
						</c:forEach>
					</div>
				</div>
				<div class="card mb-2" style="width: 100%;">
					<div class="card-header">
						<p class="h3 mt-3 p-3 my-auto bg-light text-success rounded">目次</p>
					</div>
					<div class="card-body">
						<c:forEach var="line"
							items="${fn:split(courseInformation.contents, newLineChar)}">
							<c:out value="${line}" />
							<br />
						</c:forEach>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>