<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ログイン</title>
	<%@ include file="header.jsp"%>
</head>
<body>
	<div class="text-center">
		<img class="mt-5" src="img/CodeTrain.png" width="15%">
		<p class="text-danger">
			<c:if test="${not empty message}">
				<c:out value="${message }"/>
			</c:if>
		</p>
		<div class="mx-auto">
			<form action="mo-login" class="mx-auto" method="post">
				<div class="form-row">
					<div class="col-4"></div>
					<div class="col-4">
						<input type="email" name="id" class="form-control mt-5" placeholder="メールアドレス">
					</div>
				</div>
				<div class="form-row">
					<div class="col-4"></div>
					<div class="col-4">
						<input type="password" name="password" class="form-control mt-3" placeholder="パスワード">			
					</div>
				</div>
				<button type="submit" class="btn btn-outline-primary mt-4">ログイン</button>
			</form>
		</div>
	</div>
</body>
</html>