<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>法人一覧</title>
<%@ include file="header.jsp"%>
<style>
.bg-light {
 background-color: #CCFF99!important;
}
</style>
<style>
thead {
 background-color: #CCFF99!important;
}
</style>
<style>.table-hover tbody tr:hover td, .table-hover tbody tr:hover th 
{
  background-color: #CCFF99;
}
</style>
</head>
<body>
<%@ include file="manager-operator-navbar.jsp"%>
		<h1 class="mt-3 p-3 bg-light text-success rounded">法人一覧</h1>
	<div class="container">
		<div class="row">
			<div class="mx-auto text-primary">
				&nbsp;
				<c:out value="${ message }" />
			</div>
		</div>
		<table class="table table-hover">
			<thead class="text-secondary text-center">
				<th><font color="green">メール</font></th>
				<th><font color="green">役職</font></th>
				<th><font color="green">担当者様氏名</font></th>
				<th><font color="green">ドメイン</font></th>
				<th><font color="green">請求先</font></th>
			</thead>
			<c:forEach items="${ managerList }" var="managerDto">
				<tbody  class="text-center">
					<td><c:out value="${ managerDto.email }" /></td>
					<td><c:out value="${ managerDto.position }" /></td>
					<td><c:out value="${ managerDto.name }" /></td>
					<td><c:out value="${ managerDto.domain }" /></td>
					<td><c:out value="${ managerDto.billingAddress }" /></td>
				</tbody>
			</c:forEach>
		</table>
		<c:choose>
			<c:when test="${ authority == 'HIGH' }">
				<table>
				<tr>
				<td>
				<form action="list-operator" method="get">
					<button type="submit" class="btn btn-outline-success ">運営担当者一覧</button>
				</form>
				</td>
				<td>
				<form action="list-course-operator" method="get">
					<button type="submit" class="btn btn-outline-success ">コース一覧</button>
				</form>
				</td>
				<td>
				<form action="add-manager" method="get">
					<button type="submit" class="btn btn-outline-success ">法人アカウント作成</button>
				</form>
				</td>
				</tr>
				</table>
				<br>
			</c:when>
			<c:when test="${ authority == 'MIDDLE' }">
				<table>
				<tr>
				<td>
				<form action="list-course-operator" method="get">
					<button type="submit" class="btn btn-outline-success ">コース一覧</button>
				</form>
				</td>
				<td>
				<form action="add-manager" method="get">
					<button type="submit" class="btn btn-outline-success ">法人アカウント作成</button>
				</form>
				</td>
				</tr>
				</table>
				
				<br>
			</c:when>
			<c:otherwise>
				<form action="list-course-operator" method="get">
					<button type="submit" class="btn btn-outline-success ">コース一覧</button>
				</form>
				<br>
			</c:otherwise>
		</c:choose>
		<a href="edit-password">
			<button class="btn btn-success float-right">パスワードを変更</button>
		</a>
	</div>
</body>
</html>