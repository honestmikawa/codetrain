<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	String includePath;
if (session.getAttribute("user") != null) {
	includePath = "navbar.jsp";
} else {
	includePath = "manager-operator-navbar.jsp";
}
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>パスワード変更</title>
<%@ include file="header.jsp"%>
<c:choose>
	<c:when test="${not empty operator}">
		<style>
			.bg-light {
				background-color: #CCFF99 !important;
			}
		</style>
		<style>
			label {
				color: green !important;
			}
		</style>
		<style>
			h3 {
				color: green !important;
			}
		</style>
	</c:when>
	<c:when test="${not empty manager}">
		<style>
			.bg-light {
				background-color: #F5FFFA !important;
			}
		</style>
		<style>
			label {
				color: #17A2B8 !important;
			}
		</style>
		<style>
			h3 {
				color: #17A2B8 !important;
			}
		</style>
	</c:when>
	<c:otherwise>
		<style>
			.bg-light {
				background-color: #330066 !important;
			}
		</style>
		<style>
			label {
				color: #330066!important;
			}
		</style>
		<style>
			h3 {
				color: #330066!important;
			}
		</style>
	</c:otherwise>
</c:choose>
</head>
<body>
	<c:choose>
		<c:when test="${me == 'user' }">
			<%@ include file="navbar.jsp"%>
		</c:when>
		<c:otherwise>
			<%@ include file="manager-operator-navbar.jsp"%>
		</c:otherwise>
	</c:choose>
	<h3 class="ml-5">パスワード変更</h3>
	<div class="mx-auto text-center">
		<label class="text-danger"> <c:if test="${ not empty message}">
				<c:out value="${message }" />
			</c:if>
		</label>
		<form action="edit-password" method="post">
			<label class="mt-4">現在のパスワードを入力してください</label>
			<div class="form-row">
				<div class="col-4"></div>
				<div class="col-4">
					<input type="password" name="currentPassword" class="form-control"
						placeholder="現在のパスワード">
				</div>
			</div>
			<label class="mt-4">新しいパスワードを入力してください</label>
			<div class="form-row">
				<div class="col-4"></div>
				<div class="col-4">
					<input type="password" name="newPassword" class="form-control"
						placeholder="新しいパスワード">
				</div>
			</div>
			<label class="mt-4">新しいパスワードを入力してください（確認用）</label>
			<div class="form-row">
				<div class="col-4"></div>
				<div class="col-4">
					<input type="password" name="checkPassword" class="form-control"
						placeholder="確認用パスワード">
				</div>
			</div>
			<button type="submit" class="btn btn-outline-primary mt-4">パスワード変更</button>
		</form>
	</div>
</body>
</html>