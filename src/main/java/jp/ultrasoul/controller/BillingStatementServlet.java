package jp.ultrasoul.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.ultrasoul.DataSourceManager;
import jp.ultrasoul.dao.BillingDao;
import jp.ultrasoul.dao.PriceDao;
import jp.ultrasoul.dto.BillingDto;
import jp.ultrasoul.dto.ManagerDto;
import jp.ultrasoul.dto.PriceDto;

/**
 * Servlet implementation class BillingStatementServlet
 */
@WebServlet("/manager/billing-statement")
public class BillingStatementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// コネクションを取得
		try (Connection con = DataSourceManager.getConnection()) {

			// 請求情報Daoのインスタンス作成
			BillingDao bdao = new BillingDao(con);

			// 単価Daoのインスタンス作成
			PriceDao pdao = new PriceDao(con);

			// セッション情報を取得
			HttpSession session = request.getSession();

			// 管理者情報を取得
			ManagerDto dto = (ManagerDto) session.getAttribute("manager");

			// 管理者IDを取得
			int managerId = dto.getManagerId();

			// 利用年月を取得
			int time = Integer.parseInt(request.getParameter("time"));

			// 単価IDに１を指定
			int priceId = 1;

			// 管理者ID、利用年月に該当する請求情報リストを取得
			List<BillingDto> billingList = bdao.selectByManagerIdAndTime(managerId, time);

			// 単価IDに該当する単価情報リストを取得
			List<PriceDto> priceList = pdao.selectByPriceId(priceId);

			// 請求情報リストをリクエストスコープに保持
			request.setAttribute("billingList", billingList);

			// 単価情報リストをリクエストスコープに保持
			request.setAttribute("priceList", priceList);

			// 請求明細確認画面に遷移
			request.getRequestDispatcher("/WEB-INF/billing-statement.jsp").forward(request, response);

		} catch (SQLException | NamingException e) {

			// エラー画面にリダイレクト
			response.sendRedirect("../system-error.jsp");
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// doGetを呼び出す
		doGet(request, response);
	}

}
