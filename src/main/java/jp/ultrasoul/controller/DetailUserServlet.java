package jp.ultrasoul.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.ultrasoul.DataSourceManager;
import jp.ultrasoul.dao.StudyHistoryDao;
import jp.ultrasoul.dao.UserDao;
import jp.ultrasoul.dto.StudyHistoryDto;
import jp.ultrasoul.dto.UserDto;

/**
 * Servlet implementation class DetailUserServlet
 */
@WebServlet("/manager/detail-user")
public class DetailUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(StudyHistoryServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		// コネクションを取得
		try (Connection con = DataSourceManager.getConnection()) {

			// 閲覧履歴情報Daoを取得
			StudyHistoryDao sdao = new StudyHistoryDao(con);

			// 利用者情報Daoを取得
			UserDao udao = new UserDao(con);

			// 利用者IDを取得
			int userId = Integer.parseInt(request.getParameter("userId"));

			// 利用者IDに該当する学習履歴情報リストを取得
			List<StudyHistoryDto> StudyHistoryList = sdao.selectByUserId(userId);

			// 利用者IDに該当するユーザー情報リストを取得
			UserDto userDto = udao.selectByUserId(userId);

			// リクエストスコープに学習履歴情報を保持
			request.setAttribute("StudyHistoryList", StudyHistoryList);

			// リクエストスコープにユーザー情報リストを保持
			request.setAttribute("userDto", userDto);

			// 利用者詳細にリダイレクト
			request.getRequestDispatcher("/WEB-INF/detail-user.jsp").forward(request, response);

		} catch (SQLException | NamingException e) {

			// エラー画面に遷移
			response.sendRedirect("system-error.jsp");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// doGetを呼び出す
		doGet(request, response);
	}

}
