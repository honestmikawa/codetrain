package jp.ultrasoul.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.ultrasoul.DataSourceManager;
import jp.ultrasoul.dao.CategoryDao;
import jp.ultrasoul.dao.CourseDao;
import jp.ultrasoul.dto.CategoryDto;

/**
 * Servlet implementation class AddCategoryServlet
 */
@WebServlet("/operator/add-category")
public class AddCategoryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// セッションを取得する
		HttpSession session = request.getSession();

		// セッションスコープ内のエラーメッセージをリクエストスコープに保持する
		request.setAttribute("errorMessageList", session.getAttribute("errorMessageList"));

		// セッションスコープ内からエラーメッセージを削除する
		session.removeAttribute("errorMessageList");

		// セッションスコープ内からメッセージを削除する
		session.removeAttribute("message");

		// 追加画面からの遷移かを判定
		if (request.getParameter("isAdd") != null) {
			// 追加画面からの遷移なら追加モードフラグを持っておく
			session.setAttribute("isAdd", true);
		} else {
			if (session.getAttribute("courseId") != null) {
				// 編集画面からの遷移なら、編集画面に戻るときのためにコースIDを保持しておく。
				session.setAttribute("courseId", Integer.parseInt(request.getParameter("courseId")));		
			}
		}

		// カテゴリ追加画面に転送する
		request.getRequestDispatcher("/WEB-INF/view-category.jsp").forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// フォームのデータ(カテゴリ名)を取得する
		request.setCharacterEncoding("UTF-8");
		CategoryDto categoryDto = new CategoryDto();
		categoryDto.setCategoryName(request.getParameter("categoryName"));

		// フォームのデータの入力チェックをする
		// セッションを取得する
		HttpSession session = request.getSession();

		// エラーメッセージリストの作成
		List<String> errorMessageList = new ArrayList<>();

		// カテゴリ名が空の場合
		if ("".equals(categoryDto.getCategoryName())) {
			// エラーメッセージ(カテゴリ名を入力してください)を保持する
			errorMessageList.add("カテゴリ名を入力してください");
		}

		// カテゴリ名が60字を超える場合
		if (categoryDto.getCategoryName().length() > 60) {
			// エラーメッセージ(カテゴリ名は60字以内に入力してください)を保持する
			errorMessageList.add("カテゴリ名は60字以内に入力してください");
		}

		// データベースへ接続する
		try (Connection conn = DataSourceManager.getConnection()) {

			// 入力したコース名と同じカテゴリ名を取得する
			CategoryDao categoryDao = new CategoryDao(conn);
			boolean exist = categoryDao.hasCategoryName(categoryDto.getCategoryName());

			// データベース上に同じカテゴリ名が存在する場合
			if (exist) {
				// エラーメッセージ(入力したカテゴリ名はすでに存在しています)を保持する
				errorMessageList.add("入力したカテゴリ名はすでに存在しています");
			}
		} catch (SQLException | NamingException e) {
			// システムエラー画面に遷移する
			request.getRequestDispatcher("/system-error.jsp").forward(request, response);
			return;
		}

		// エラーがある場合
		if (errorMessageList.size() != 0) {
			// エラーメッセージをセッションスコープに保持する
			session.setAttribute("errorMessageList", errorMessageList);
			
			if (session.getAttribute("courseId") != null) {
				// AddCategoryServletにリダイレクトする
				response.sendRedirect("/CodeTrain/operator/add-category");
			} else {
				// 追加モードフラグ付きでAddCategoryServletにリダイレクトする
				response.sendRedirect("/CodeTrain/operator/add-category?isAdd=true");
			}
			return;
		}

		// データベースへ接続する
		try (Connection conn = DataSourceManager.getConnection()) {

			// カテゴリ名をカテゴリテーブルに追加する
			CategoryDao categoryDao = new CategoryDao(conn);
			categoryDao.insert(categoryDto);

			// メッセージ(追加しました)をセッションスコープに保持する
			session.setAttribute("message", "追加しました");

			// 追加したカテゴリ名をセッションに保持する
			session.setAttribute("newCategory", categoryDto.getCategoryName());
			// 追加モードでリダイレクト先を場合分けする
			if (session.getAttribute("isAdd") == null) {
				// EditCourseOperatorServletにリダイレクトする
				response.sendRedirect("/CodeTrain/operator/edit-course");
			} else {
				// セッションから追加モードフラグを消去する
				session.removeAttribute("isAdd");
				// AddCourseOperatorServletにリダイレクトする
				response.sendRedirect("/CodeTrain/operator/add-course");
			}

		} catch (SQLException | NamingException e) {
			// システムエラー画面に転送する
			e.printStackTrace();
			request.getRequestDispatcher("/system-error.jsp").forward(request, response);
		}

	}

}
