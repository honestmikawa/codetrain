
package jp.ultrasoul.controller;

import java.io.IOException;


import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.ultrasoul.DataSourceManager;
import jp.ultrasoul.dao.OperatorDao;
import jp.ultrasoul.dto.OperatorDto;

/**
 * Servlet implementation class EditOperatorServlet
 */
@WebServlet("/operator/edit-operator")
@MultipartConfig(maxFileSize=1048576)
public class EditOperatorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//ListOperatorServletにリダイレクトする
		response.sendRedirect("/CodeTrain/operator/list-operator");

	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//フォームのデータを取得する
		request.setCharacterEncoding("UTF-8");
		OperatorDto operatorDto = new OperatorDto();
		//運営者ID
		operatorDto.setOperatorId(Integer.parseInt(request.getParameter("operatorId")));
		//氏名
		operatorDto.setName(request.getParameter("operatorName"));
		//権限
		operatorDto.setAuthority(request.getParameter("authority"));
		
		//フォームのデータの入力チェックをする
		// セッションを取得する
		HttpSession session = request.getSession();
		// エラーメッセージリストの作成
		List<String> errorMessageList = new ArrayList<>();
		
		//氏名が空の場合
		if ("".equals(operatorDto.getName())) {
			//エラーメッセージ(氏名を入力してください)を保持する
			errorMessageList.add("氏名を入力してください");
		}	
		
		//氏名が30字を超える場合
		if (operatorDto.getName().length() > 30) {
			//エラーメッセージ(30字以内に入力してください)を保持する
			errorMessageList.add("30字以内に入力してください");
		}
		
		// エラーがある場合
		if (errorMessageList.size() != 0) {
			// エラーメッセージをセッションスコープに保持する
			session.setAttribute("errorMessageList", errorMessageList);
			// ListOperatorServletにリダイレクトする
			response.sendRedirect("/CodeTrain/operator/list-operator");
			return;
		}
		
		// データベースへ接続する
		try (Connection conn = DataSourceManager.getConnection()) {
			
		//運営者IDに紐づく氏名、権限を更新する
		OperatorDao operatorDao = new OperatorDao(conn);
		operatorDao.update(operatorDto);
		
		//メッセージ(保存しました)をセッションスコープに保持する
		session.setAttribute("message", "保存しました");
		
		//ListOperatorServletにリダイレクトする
		response.sendRedirect("/CodeTrain/operator/list-operator");
		
		} catch (SQLException | NamingException e) {
			// システムエラー画面に転送する
			request.getRequestDispatcher("/system-error.jsp").forward(request, response);
		}

	}

}