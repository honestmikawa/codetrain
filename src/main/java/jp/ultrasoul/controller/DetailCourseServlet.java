package jp.ultrasoul.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mysql.cj.Session;

import jp.ultrasoul.DataSourceManager;
import jp.ultrasoul.dao.CourseDao;
import jp.ultrasoul.dto.CourseDto;

/**
 * Servlet implementation class DetailCourseServlet
 */
@WebServlet("/detail-course")
public class DetailCourseServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(DetailCourseServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("コース詳細アクセス");

		// コネクションを取得
		try (Connection con = DataSourceManager.getConnection()) {

			// コースDaoのインスタンス作成
			CourseDao dao = new CourseDao(con);

			// コースIDを取得
			int courseId = Integer.parseInt(request.getParameter("courseId"));

			// コースIDに該当するコース情報を取得
			CourseDto courseInformation = dao.selectByCourseId(courseId);

			// セッションを取得
			HttpSession session = request.getSession();

			// フリーコースでない場合かつ、セッションにユーザー情報がない場合コース一覧に遷移
			if (!courseInformation.isFreeFlg() && session.getAttribute("user") == null) {
				response.sendRedirect("list-course");
				return;
			}

			// コース情報をリクエストスコープに保持
			request.setAttribute("courseInformation", courseInformation);

			// URIをリクエストに保持する
			request.setAttribute("uri", request.getRequestURI() + "?courseId=" + courseId);

			// リクエストスコープにナブバーメッセージを保持、セッションスコープからは削除
			request.setAttribute("nabvarMessage", session.getAttribute("navbarMessage"));
			session.removeAttribute("message");

			// コース詳細にリダイレクト
			request.getRequestDispatcher("WEB-INF/detail-course.jsp").forward(request, response);

		} catch (SQLException | NamingException e) {

			// エラー画面に遷移
			response.sendRedirect("system-error.jsp");

		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// doGetを呼び出す
		doGet(request, response);
	}

}
