package jp.ultrasoul.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.ultrasoul.DataSourceManager;
import jp.ultrasoul.dao.OperatorDao;
import jp.ultrasoul.dto.OperatorDto;

/**
 * Servlet implementation class ListOperatorServlet
 */
@WebServlet("/operator/list-operator")
public class ListOperatorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private Logger logger = LoggerFactory.getLogger(ListOperatorServlet.class);


	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
		
		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());
	
		 try(Connection con =  DataSourceManager.getConnection()){
			 
			// メッセージをリクエストに保持する
			 request.setAttribute("message", session.getAttribute("message"));
			 session.removeAttribute("message");
			 
			// セッションスコープ内のエラーメッセージをリクエストスコープに保持する
			request.setAttribute("errorMessageList", session.getAttribute("errorMessageList"));
		
			// セッションスコープ内からエラーメッセージを削除する
			session.removeAttribute("errorMessageList");
			 
			 // ログイン状態を確認する
			 request.setAttribute("operator",session.getAttribute("operator"));
			 
			 // operator listを取得する
			OperatorDao operatorDao = new OperatorDao(con);
			 List<OperatorDto> operatorList = operatorDao.selectAll();
			 
			 request.setAttribute("operatorList", operatorList);
			 
			 //list-operator.jspに遷移する
			 request.getRequestDispatcher("/WEB-INF/list-operator.jsp").forward(request, response);
			 
		 } catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
