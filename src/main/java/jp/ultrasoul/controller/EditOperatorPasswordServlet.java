package jp.ultrasoul.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.ultrasoul.DataSourceManager;
import jp.ultrasoul.dao.OperatorDao;
import jp.ultrasoul.dto.OperatorDto;

/**
 * Servlet implementation class EditUserPasswordServlet
 */
@WebServlet("/operator/edit-password")
public class EditOperatorPasswordServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		// セッションにメッセージがあればリクエストスコープに保持し、セッションから削除
		if (session.getAttribute("message") != null) {
			request.setAttribute("message", session.getAttribute("message"));
			session.removeAttribute("message");
		}
		
		// パスワード変更者の識別子を保持
		request.setAttribute("me", "operator");
		
		// パスワード変更画面に遷移
		request.getRequestDispatcher("../WEB-INF/view-password.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		OperatorDto operator = (OperatorDto) session.getAttribute("operator");
		// フォームから現在のパスワード、新しいパスワード、確認用パスワードを取得する
		String currentPassword = request.getParameter("currentPassword");
		String newPassword = request.getParameter("newPassword");
		String checkPassword = request.getParameter("checkPassword");
		
		// 入力チェック
		String message = "";
		if ("".equals(currentPassword) || "".equals(newPassword) || "".equals(checkPassword)) {
			message = "全ての項目を入力してください";
		} else if (currentPassword.length() > 30 || newPassword.length() > 30){
			message = "パスワードは30字以内で入力してください";
		} else if (!newPassword.equals(checkPassword)) {
			message = "確認用のパスワードが異なっています";
		}
		// 入力チェックでエラーがあればパスワード変更画面に遷移
		if (!"".equals(message)) {
			request.setAttribute("message", message);
			request.getRequestDispatcher("../WEB-INF/view-password.jsp").forward(request, response);
			return;
		}
		
		try (Connection con = DataSourceManager.getConnection()) {
			// 利用者表のパスワードを更新
			OperatorDao operatorDao = new OperatorDao(con);
			int result = operatorDao.updatePassword(operator.getOperatorId(), currentPassword, newPassword);
			
			// 更新していなければパスワード更新画面に遷移
			if (result == 0) {
				request.setAttribute("message", "現在のパスワードが間違っています");
				request.getRequestDispatcher("../WEB-INF/view-password.jsp").forward(request, response);
				return;
			}
			
			// 更新できたらコース一覧に遷移
			session.setAttribute("message", "パスワードを変更しました。");
			response.sendRedirect("list-manager");
		} catch (SQLException | NamingException e) {
			// システムエラー画面に遷移する
			response.sendRedirect("system-error.jsp");
		}
	}

}
