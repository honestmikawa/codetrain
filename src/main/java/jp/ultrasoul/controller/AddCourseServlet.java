package jp.ultrasoul.controller;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.ultrasoul.DataSourceManager;
import jp.ultrasoul.dao.CourseDao;
import jp.ultrasoul.dao.CategoryDao;
import jp.ultrasoul.dto.CategoryDto;
import jp.ultrasoul.dto.CourseDto;

/**
 * Servlet implementation class AddCourseServlet
 */
@WebServlet("/operator/add-course")
@SuppressWarnings("serial")
@MultipartConfig(maxFileSize=10485760) 
public class AddCourseServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private Logger logger = LoggerFactory.getLogger(AddCourseServlet.class);
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		// セッションを取得する
		HttpSession session = request.getSession();
				
		try (Connection conn = DataSourceManager.getConnection()) {
		
			//カテゴリ情報リストを取得し、リクエストスコープに保持する
			CategoryDao categoryDao = new CategoryDao(conn);
			List<CategoryDto> list = categoryDao.selectAll();
			request.setAttribute("list", list);

			// リクエストスコープに追加モードフラグを保持する
			request.setAttribute("isAdd", true);
	
			// セッションスコープ内のエラーメッセージをリクエストスコープに保持する
			request.setAttribute("errorMessageList", session.getAttribute("errorMessageList"));
				
			// セッションスコープ内からエラーメッセージを削除する
			session.removeAttribute("errorMessageList");
				
			// セッションスコープ内からメッセージを削除する
			session.removeAttribute("message");
			
			//
			if(session.getAttribute("isAdd") != null) {
				//セッションスコープ内の追加したカテゴリ名をリクエストスコープに保持する
				request.setAttribute("newCategory", session.getAttribute("newCategory"));
				
				//セッションスコープ内から追加したカテゴリ名を削除する
				session.removeAttribute("newCategory");
			}
			// コース追加画面に転送する
			request.getRequestDispatcher("/WEB-INF/view-course.jsp").forward(request, response);
			
		} catch (SQLException | NamingException e) {
			// システムエラー画面に遷移する
			request.getRequestDispatcher("/system-error.jsp").forward(request, response);
		} catch (Exception e) {
			// システムエラー画面に遷移する
			request.getRequestDispatcher("/system-error.jsp").forward(request, response);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// フォームのデータを取得する
		request.setCharacterEncoding("UTF-8");
		CourseDto courseDto = new CourseDto();
		// カテゴリID
		courseDto.setCategoryId(Integer.parseInt(request.getParameter("categoryId")));
		// フリーコース
		boolean freeFlg = request.getParameter( "freeFlg" ) != null;
		courseDto.setFreeFlg(freeFlg);
		// コース名
		courseDto.setCourseName(request.getParameter("courseName"));
		// 学習目安時間
		String estimatedTime = request.getParameter("estimatedTime");
		if (!"".equals(estimatedTime)) {
			courseDto.setEstimatedTime(Integer.parseInt(estimatedTime));
		}
		// 前提条件
		courseDto.setPrecondition(request.getParameter("precondition"));
		// 概要
		courseDto.setOverview(request.getParameter("overview"));
		// ゴール
		courseDto.setGoal(request.getParameter("goal"));
		// 目次
		courseDto.setContents(request.getParameter("contents"));
		// PDF
		Part fPart = request.getPart("pdf");
		
		// フォームのデータの入力チェックをする
		// セッションを取得する
		HttpSession session = request.getSession();
		// エラーメッセージリストの作成
		List<String> errorMessageList = new ArrayList<>();
		
		// コース名、前提条件、学習目安時間、概要、目次、ゴールが空の場合
		if ("".equals(courseDto.getCourseName()) || "".equals(courseDto.getPrecondition())
			|| "".equals(courseDto.getOverview()) || "".equals(courseDto.getGoal())
			|| "".equals(courseDto.getContents()) || courseDto.getEstimatedTime() == 0) {
			// エラーメッセージ(すべての項目を入力してください)を保持する
			errorMessageList.add("すべての項目を入力してください");
		}
		
		//PDFが空の場合
		if ( fPart.getSize() <= 0 ) {
			// エラーメッセージ(ファイルを選択してください)を保持する
			errorMessageList.add("ファイルを選択してください");
		}
		
		// コース名が30字を超える場合
		if (courseDto.getCourseName().length() > 30) {
			// エラーメッセージ(コース名は30字以内に入力してください)を保持する
			errorMessageList.add("コース名は30字以内に入力してください");
		}
		
		// 学習目安時間が11字を超える場合
		if (String.valueOf(courseDto.getEstimatedTime()).length() > 11) {
			// エラーメッセージ(学習目安時間は11字以内に入力してください)を保持する
			errorMessageList.add("学習目安時間は11字以内に入力してください");
		}
		
		// 前提条件、概要、ゴール、目次のいずれかが255字を超える場合
		if (courseDto.getPrecondition().length() > 255 || courseDto.getOverview().length() > 255
			|| courseDto.getGoal().length() > 255 || courseDto.getContents().length() > 255) {
			// エラーメッセージ(前提条件、概要、ゴール、目次は255字以内に入力してください)を保持する
			errorMessageList.add("前提条件、概要、ゴール、目次は255字以内に入力してください");
		}
		
		//データベースへ接続する
		try (Connection conn = DataSourceManager.getConnection()) {

			//入力したコース名と同じコース名を取得する
			CourseDao courseDao = new CourseDao(conn);
			int result = courseDao.selectByCourseName(courseDto.getCourseName());

			//データベース上に同じコース名が存在する場合
			if (result == 1) {
				// エラーメッセージ(入力したコース名はすでに存在しています)を保持する
				errorMessageList.add("入力したコース名はすでに存在しています");
			}
			
		} catch (SQLException | NamingException e) {
			// システムエラー画面に遷移する
			request.getRequestDispatcher("/system-error.jsp").forward(request, response);
			return;
		} catch (Exception e) {
			// システムエラー画面に遷移する
			request.getRequestDispatcher("/system-error.jsp").forward(request, response);
			return;
		}

		// エラーがある場合
		if (errorMessageList.size() != 0) {
			// エラーメッセージをセッションスコープに保持する
			session.setAttribute("errorMessageList", errorMessageList);
			// AddCourseServletにリダイレクトする
			response.sendRedirect("/CodeTrain/operator/add-course");
			return;
		}
		
		// 次に付与されるコースIDを取得する
		int nextCourseId = 0;
		try (Connection conn = DataSourceManager.getConnection()) {
			CourseDao courseDao = new CourseDao(conn);
			nextCourseId = courseDao.maxCourseId() + 1;
			courseDto.setCourseId(nextCourseId);
			
		} catch (SQLException | NamingException e) {
			e.printStackTrace();
			// システムエラー画面に遷移する
			request.getRequestDispatcher("/system-error.jsp").forward(request, response);
			return;
		}
		
		// PDFに”Course(nextCourseId).pdf”という名前を付けてPDFのパスを作成する
		String fName = "course" + nextCourseId + ".pdf";
		String fDir = getServletContext().getRealPath("pdf");
		
		File file = new File(fDir + "/" + fName);
		// ファイルの存在を確認
        Boolean fileExists = file.exists();
        // PDFのパスがすでに存在していた場合
        if (fileExists) {
        	// エラーメッセージ(ファイルがすでに存在しています)を保持する
            errorMessageList.add("ファイルがすでに存在しています");
        }
        
		// エラーがある場合
		if (errorMessageList.size() != 0) {
			// エラーメッセージをセッションスコープに保持する
			session.setAttribute("errorMessageList", errorMessageList);
			// AddCourseServletにリダイレクトする
			response.sendRedirect("/CodeTrain/operator/add-course");
			return;
		}

    	//PDFを保存する
		File uplodeDir = new File(fDir);
		uplodeDir.mkdir();
    	save(fPart, new File(fDir, fName));
    	courseDto.setPdfPath("pdf/" + fName);

		// PDFの総ページ数を取得する
		PDDocument document = PDDocument.load(file);
        int pages = document.getNumberOfPages();
        courseDto.setTotalPages(pages);
        
		// データベースへ接続する
		try (Connection conn = DataSourceManager.getConnection()) {

			// カテゴリID、前提条件、コース名、学習目安時間、概要、ゴール、目次、PDFのパス、PDFページ数を学習コーステーブルに追加する
			CourseDao courseDao = new CourseDao(conn);
			courseDao.insert(courseDto);

			// メッセージ(追加しました)をセッションスコープに保持する
			session.setAttribute("message", "追加しました");
			
			// リクエストスコープに追加モードフラグを保持する
			request.setAttribute("isAdd", true);

			// リクエストスコープに続けて追加モードフラグがある場合
			if (request.getParameter("continueAdd") != null) {
				// view-course.jspに転送する
				request.getRequestDispatcher("/WEB-INF/view-course.jsp").forward(request, response);
			// リクエストスコープに続けて追加モードフラグがない場合
			} else {
				// ListCourseOperatorServletにリダイレクトする
				response.sendRedirect("/CodeTrain/operator/list-course-operator");
			}

		} catch (SQLException | NamingException e) {
			// システムエラー画面に転送する
			e.printStackTrace();
			request.getRequestDispatcher("/system-error.jsp").forward(request, response);
		}

	}

	//saveメソッド
	public void save(Part in, File out) throws IOException {
	    BufferedInputStream br = new BufferedInputStream(in.getInputStream());
	    try (BufferedOutputStream bw = new BufferedOutputStream(new FileOutputStream(out))) {
	    	int len = 0;
	    	byte[] buff = new byte[1024];
	    	while ((len = br.read(buff)) != -1) {
	    		bw.write(buff, 0, len);
	        }
	    }	    
	}

}