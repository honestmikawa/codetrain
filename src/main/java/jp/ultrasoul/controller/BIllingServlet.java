package jp.ultrasoul.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.ultrasoul.DataSourceManager;
import jp.ultrasoul.dao.BillingDao;
import jp.ultrasoul.dao.UserDao;
import jp.ultrasoul.dto.BillingDto;
import jp.ultrasoul.dto.ManagerDto;

/**
 * Servlet implementation class BIllingServlet
 */
@WebServlet("/manager/billing")
public class BIllingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(DetailCourseServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		logger.info("請求確認機能アクセス");

		// コネクションを取得
		try (Connection con = DataSourceManager.getConnection()) {

			// 請求情報Daoのインスタンス作成
			BillingDao dao = new BillingDao(con);

			// セッション情報を取得
			HttpSession session = request.getSession();

			// 管理者情報を取得
			ManagerDto dto = (ManagerDto) session.getAttribute("manager");

			// 管理者IDを取得
			int managerId = dto.getManagerId();

			// 管理者IDに該当する請求明細情報リストを取得
			List<BillingDto> billingList = dao.selectByManagerId(managerId);

			// 請求明細情報リストをリクエストスコープに保持
			request.setAttribute("billingList", billingList);

			// 請求確認画面に遷移
			request.getRequestDispatcher("/WEB-INF/billing.jsp").forward(request, response);

		} catch (SQLException | NamingException e) {

			// エラー画面に遷移
			response.sendRedirect("../system-error.jsp");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// doGetを呼び出す
		doGet(request, response);
	}

}
