package jp.ultrasoul.controller;

import java.io.BufferedInputStream;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import org.apache.pdfbox.pdmodel.PDDocument;

import jp.ultrasoul.DataSourceManager;
import jp.ultrasoul.dao.CategoryDao;
import jp.ultrasoul.dao.CourseDao;
import jp.ultrasoul.dto.CategoryDto;
import jp.ultrasoul.dto.CourseDto;

/**
 * Servlet implementation class EditCourseServlet
 */
@WebServlet("/operator/edit-course")
@SuppressWarnings("serial")
@MultipartConfig(maxFileSize=1048576) 
public class EditCourseServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//セッションを取得する
		HttpSession session = request.getSession();
		
		// セッションスコープ内のエラーメッセージをリクエストスコープに保持する
		request.setAttribute("errorMessageList", session.getAttribute("errorMessageList"));

		// セッションスコープ内からエラーメッセージを削除する
		session.removeAttribute("errorMessageList");
		
		// セッションスコープ内からメッセージを削除する
		session.removeAttribute("message");
		
		String courseId = request.getParameter("courseId");
		
		int courseid = 0;
		
		if(courseId != null) {
			courseid = Integer.parseInt(courseId);
		}else {
			courseid = (int)session.getAttribute("courseId");
			session.removeAttribute("courseId");
		}
		
		if(session.getAttribute("newCategory") != null) {
			//セッションスコープ内の追加したカテゴリ名をリクエストスコープに保持する
			request.setAttribute("newCategory", session.getAttribute("newCategory"));
			
			//セッションスコープ内から追加したカテゴリ名を削除する
			session.removeAttribute("newCategory");
		}

		try (Connection conn = DataSourceManager.getConnection()) {
			
			
			//リクエストスコープ内のコースIDを取得し、コースIDに紐づくコース情報を取得する
			request.setCharacterEncoding("UTF-8");
			CourseDao courseDao = new CourseDao(conn);
			CourseDto courseDto = courseDao.selectByCourseId(courseid);
			
			//セッション
			
			//取得したコース情報をリクエストスコープに保持する
			request.setAttribute("courseDto", courseDto);
			
			//カテゴリ情報リストを取得し、リクエストスコープに保持する
			CategoryDao categoryDao = new CategoryDao(conn);
			List<CategoryDto> list = categoryDao.selectAll();
			request.setAttribute("list", list);
				
			//コース追加画面に転送する
			request.getRequestDispatcher("/WEB-INF/view-course.jsp").forward(request, response);
			
		} catch (SQLException | NamingException e) {
			// システムエラー画面に遷移する
			request.getRequestDispatcher("/system-error.jsp").forward(request, response);
		} catch (Exception e) {
			// システムエラー画面に遷移する
			request.getRequestDispatcher("/system-error.jsp").forward(request, response);
		}
			
	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//コースIDをリクエストスコープから取得する
		request.setCharacterEncoding("UTF-8");
		int courseId = Integer.parseInt(request.getParameter("courseId"));
		
		//フォームのデータを取得する
		request.setCharacterEncoding("UTF-8");
		CourseDto courseDto = new CourseDto();
		//コースID
		courseDto.setCourseId(courseId);
		// カテゴリID
		courseDto.setCategoryId(Integer.parseInt(request.getParameter("categoryId")));
		// フリーコース
		boolean freeFlg = request.getParameter( "freeFlg" ) != null;
		courseDto.setFreeFlg(freeFlg);
		// コース名
		courseDto.setCourseName(request.getParameter("courseName"));
		// 学習目安時間
		String estimatedTime = request.getParameter("estimatedTime");
		if (!"".equals(estimatedTime)) {
			courseDto.setEstimatedTime(Integer.parseInt(estimatedTime));
		}
		// 前提条件
		courseDto.setPrecondition(request.getParameter("precondition"));
		// 概要
		courseDto.setOverview(request.getParameter("overview"));
		// ゴール
		courseDto.setGoal(request.getParameter("goal"));
		// 目次
		courseDto.setContents(request.getParameter("contents"));
		// PDF
		Part fPart = request.getPart("pdf");
		
		//フォームのデータの入力チェックをする
		//セッションを取得する
		HttpSession session = request.getSession();
		//エラーメッセージリストの作成
		List<String> errorMessageList = new ArrayList<>();
		//コース名、前提条件、概要、目次、ゴールが空の場合
		if ("".equals(courseDto.getCourseName()) || "".equals(courseDto.getPrecondition())
			|| "".equals(courseDto.getOverview()) || "".equals(courseDto.getGoal())
			|| "".equals(courseDto.getContents()) || courseDto.getEstimatedTime() == 0) {
			//エラーメッセージ(すべての項目を入力してください)を保持する
			errorMessageList.add("すべての項目を入力してください");
		}
		//コース名が30字を超える場合
		if (courseDto.getCourseName().length() > 30) {
			//エラーメッセージ(コース名は30字以内に入力してください)を保持する
			errorMessageList.add("コース名は30字以内に入力してください");
		}	
		
		// 学習目安時間が11字を超える場合
		if (String.valueOf(courseDto.getEstimatedTime()).length() > 11) {
			// エラーメッセージ(学習目安時間は11字以内に入力してください)を保持する
			errorMessageList.add("学習目安時間は11字以内に入力してください");
		}
		
		//前提条件、概要、ゴール、目次のいずれかが255字を超える場合
		if (courseDto.getPrecondition().length() > 255 || courseDto.getOverview().length() > 255
			|| courseDto.getGoal().length() > 255 || courseDto.getContents().length() > 255) {
			//エラーメッセージ(前提条件、概要、ゴール、目次は255字以内に入力してください)を保持する
			errorMessageList.add("前提条件、概要、ゴール、目次は255字以内に入力してください");
		}
		
		//エラーがある場合
		if (errorMessageList.size() != 0) {
			//エラーメッセージをセッションスコープに保持する
			session.setAttribute("errorMessageList", errorMessageList);
			//EditCourseServletにリダイレクトする
			response.sendRedirect("/CodeTrain/operator/edit-course?courseId=" + courseId);
			return;
		}
		
		//フォームのデータにPDFがある場合
		if ( fPart.getSize() > 0 ) {
			
			//PDFに元と同じPDFのパスの名前をつける
			String fName = "course" + courseId + ".pdf";
			String fDir = getServletContext().getRealPath("pdf");
			
			File file = new File(fDir + "/" + fName);

	    	//PDFを置き換える
			File uplodeDir = new File(fDir);
			uplodeDir.mkdir();
	    	save(fPart, new File(fDir, fName));
	    	courseDto.setPdfPath("pdf/" + fName);
	    	
			//PDFページ数を取得する
			PDDocument document = PDDocument.load(file);
	        int pages = document.getNumberOfPages();
	        courseDto.setTotalPages(pages);
	        
	        try (Connection conn = DataSourceManager.getConnection()) {

				//カテゴリID、前提条件、コース名、学習目安時間、概要、ゴール、目次、PDFのパス、PDFページ数を更新する
				CourseDao courseDao = new CourseDao(conn);
				courseDao.updateWithPDF(courseDto);
				
			} catch (SQLException | NamingException e) {
				//システムエラー画面に転送する
				request.getRequestDispatcher("system-error.jsp").forward(request, response);
			}
	        
	    //フォームのデータにPDFがない場合
		} else {
			
	        //データベースへ接続する
			try (Connection conn = DataSourceManager.getConnection()) {

				//カテゴリID、前提条件、コース名、学習目安時間、概要、ゴール、目次を更新する
				CourseDao courseDao = new CourseDao(conn);
				courseDao.update(courseDto);
				
			} catch (SQLException | NamingException e) {
				//システムエラー画面に転送する
				request.getRequestDispatcher("system-error.jsp").forward(request, response);
			}
		}

		//メッセージ(更新しました)をセッションスコープに保持する
		session.setAttribute("message", "更新しました");
		
		//カテゴリIDをパラメータに付与し、ListCourseServletにリダイレクトする
		response.sendRedirect("/CodeTrain/operator/list-course-operator?courseId=" + request.getParameter("categoryId"));
	}
		
	
	//saveメソッド
	public void save(Part in, File out) throws IOException {
		BufferedInputStream br = new BufferedInputStream(in.getInputStream());
		try (BufferedOutputStream bw = new BufferedOutputStream(new FileOutputStream(out))) {
		    int len = 0;
		    byte[] buff = new byte[1024];
		    while ((len = br.read(buff)) != -1) {
		    	bw.write(buff, 0, len);
		    }
		}	    
	}
}