package jp.ultrasoul.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.ultrasoul.DataSourceManager;
import jp.ultrasoul.dao.StudyHistoryDao;
import jp.ultrasoul.dto.StudyHistoryDto;
import jp.ultrasoul.dto.UserDto;

/**
 * Servlet implementation class StudyHistoryServlet
 */
@WebServlet("/study-history")
public class StudyHistoryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger logger = LoggerFactory.getLogger(StudyHistoryServlet.class);

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		UserDto user = (UserDto) session.getAttribute("user");
		if (user == null) {
			response.sendRedirect("list-course");
			return;
		}
		StudyHistoryDto dto = new StudyHistoryDto();
		dto.setUserId(user.getUserId());
		dto.setCourseId(Integer.parseInt(request.getParameter("courseId")));
		dto.setLastPage(Integer.parseInt(request.getParameter("lastPage")));
		dto.setMaxPage(Integer.parseInt(request.getParameter("maxPage")));

		try (Connection con = DataSourceManager.getConnection()) {
			StudyHistoryDao dao = new StudyHistoryDao(con);
			dao.update(dto);
		} catch (SQLException | NamingException e) {
			logger.error("{} {}", e.getClass(), e.getMessage());
		}
	}

}
