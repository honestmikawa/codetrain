package jp.ultrasoul.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.ultrasoul.DataSourceManager;
import jp.ultrasoul.dao.AnswerDao;
import jp.ultrasoul.dao.CourseDao;
import jp.ultrasoul.dao.QuestionDao;
import jp.ultrasoul.dto.AnswerDto;
import jp.ultrasoul.dto.CourseDto;
import jp.ultrasoul.dto.QuestionDto;

/**
 * Servlet implementation class ListQuestionOperatorServlet
 */
@WebServlet("/operator/list-question")
public class ListQuestionOperatorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private Logger logger = LoggerFactory.getLogger(ListQuestionOperatorServlet.class);
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());
		
		// セッションにメッセージがあればリクエストスコープに保持して、セッションから削除
		if(session.getAttribute("message") != null) {
			request.setAttribute("message", session.getAttribute("message"));
			session.removeAttribute("message");
		}
		
		// セッションに送信成功メッセージがあればリクエストスコープに保持して、セッションから削除
		if(session.getAttribute("successMessage") != null) {
			request.setAttribute("successMessage", session.getAttribute("successMessage"));
			session.removeAttribute("successMessage");
		}
		
		// リクエストからコースIDを取得する
		int courseId = Integer.parseInt(request.getParameter("courseId"));
		try (Connection con = DataSourceManager.getConnection()) {
			// コース名を取得し、リクエストスコープに保持
			CourseDao courseDao = new CourseDao(con);
			CourseDto courseDto = courseDao.selectByCourseId(courseId);
			request.setAttribute("courseName", courseDto.getCourseName());
			
			// コースIDに紐づく質問リストを取得し、リクエストスコープに保持
			QuestionDao questionDao = new QuestionDao(con);
			List<QuestionDto> questionList = questionDao.selectByCourseId(courseId);
			request.setAttribute("questionList", questionList);
			
			//　コースIDに紐づく回答リストを取得し、リクエストスコープに保持
			AnswerDao answerDao = new AnswerDao(con);
			List<AnswerDto> answerList = answerDao.selectByCourseId(courseId);
			request.setAttribute("answerList", answerList);
			
			// list-question-operator.jspにフォワード
			request.getRequestDispatcher("/WEB-INF/list-question-operator.jsp").forward(request, response);;
		} catch (SQLException | NamingException e) {
			logger.error("{} {}", e.getClass(), e.getMessage());
			request.getRequestDispatcher("system-error.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
