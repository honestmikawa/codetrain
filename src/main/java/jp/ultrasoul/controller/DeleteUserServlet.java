package jp.ultrasoul.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.ultrasoul.DataSourceManager;
import jp.ultrasoul.dao.StudyHistoryDao;
import jp.ultrasoul.dao.UserDao;
import jp.ultrasoul.dto.StudyHistoryDto;
import jp.ultrasoul.dto.UserDto;

/**
 * Servlet implementation class DeleteUserServlet
 */
@WebServlet("/manager/delete-user")
public class DeleteUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Logger logger = LoggerFactory.getLogger(EditUserServlet.class);
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("削除");
		//利用者一覧ページに遷移する
		request.getRequestDispatcher("/manager/list-user").forward(request, response);
				
			
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// セッションを取得する
			HttpSession session = request.getSession(false);
				
		// フォームのデータを取得する
				request.setCharacterEncoding("UTF-8");
				UserDto userDto = new UserDto();
				userDto.setUserId(Integer.parseInt(request.getParameter("userId")));
				StudyHistoryDto studyHistoryDto = new StudyHistoryDto();
				studyHistoryDto.setUserId(Integer.parseInt(request.getParameter("userId")));

		// コネクションを取得する
		try (Connection conn = DataSourceManager.getConnection()) {
			
			StudyHistoryDao studyHistoryDao = new StudyHistoryDao(conn);
			studyHistoryDao.delete(studyHistoryDto);
			UserDao userDao = new UserDao(conn);
			userDao.delete(userDto);
			
			//メッセージ「削除しました」を保持する
			session.setAttribute("Message", "削除しました");
			
			//DeleteUserServletに遷移する
			response.sendRedirect("delete-user");
	}catch (SQLException | NamingException e) {
		logger.error("{} {}", e.getClass(), e.getMessage());
		// システムエラー画面に遷移する
		response.sendRedirect("system-error.jsp");
	}		
	
	}

}
