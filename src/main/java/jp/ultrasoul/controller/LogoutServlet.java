package jp.ultrasoul.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Servlet implementation class LogoutServlet
 */
@WebServlet("/logout")
public class LogoutServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private Logger logger = LoggerFactory.getLogger(LogoutServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());

		String url = "list-course";
		
		// セッションを取得
		HttpSession session = request.getSession(true);
		
		// セッションに利用者情報があれば削除して遷移先URLをコース一覧に設定
		if (session.getAttribute("user") != null) {
			session.removeAttribute("user");
			url = "list-course";
		} else if (session.getAttribute("manager") != null) {
			// セッションに管理者情報があれば削除して遷移先URLをログインページに設定
			session.getAttribute("manager");
			url = "mo-login";			
		} else if (session.getAttribute("operator") != null) {
			// セッションに運用者情報があれば削除して遷移先URLをログインページに設定
			session.getAttribute("operator");
			url = "mo-login";
		}
		
		// セッションを削除
		session.invalidate();
		
		// URLに遷移
		response.sendRedirect(url);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
