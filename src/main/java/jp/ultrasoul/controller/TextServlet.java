package jp.ultrasoul.controller;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Servlet implementation class TextServlet
 */
@WebServlet("/text")
public class TextServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private Logger logger = LoggerFactory.getLogger(TextServlet.class);
       
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());
		
		// セッションにテキストを開く権利が保存されていなければコース一覧に遷移させる
		HttpSession session = request.getSession();
		if (session.getAttribute("text-ticket") == null) {
			response.sendRedirect("list-course");
			return;
		}
		
		// セッションからコースIDを取得し、対応するテキストのパスを作る。
		int courseId = (int)session.getAttribute("text-ticket");
		String fileName = "course" + courseId + ".pdf";
		String contextPath = getServletContext().getRealPath("pdf");
		File pdfFile = new File(contextPath + "/" + fileName);
		
		// PDFファイルをレスポンスとして返す
		try {
			// レスポンスのヘッダを設定
			response.reset();
			response.setHeader("Content-Disposition", "inline; filename=" + fileName);
			response.setContentType("application/pdf");
			response.setContentLength((int) pdfFile.length());
			
			// PDFファイルのインプットストリームを作成
			BufferedInputStream in = null;
			byte[] buf;
			in = new BufferedInputStream(
					new FileInputStream(pdfFile)
			);
			
			// PDFのアウトプットストリームを作成
			OutputStream out = response.getOutputStream();
			
			// PDFデータを1024バイトずつ送信
			buf = new byte[1024];
			int bytedata = 0;
			while ((bytedata = in.read(buf, 0, buf.length)) > -1) {
				out.write(buf, 0, bytedata);
			}
			out.flush();				

			// ストリームを閉じる
			in.close();
			out.close();
		} catch (FileNotFoundException e) {
			
		} catch (Exception e) {
			throw new IllegalStateException(e);
		}	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
