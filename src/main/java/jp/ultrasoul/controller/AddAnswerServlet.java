package jp.ultrasoul.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.ultrasoul.DataSourceManager;
import jp.ultrasoul.dao.AnswerDao;
import jp.ultrasoul.dto.OperatorDto;

/**
 * Servlet implementation class AddAnswerServlet
 */
@WebServlet("/operator/add-answer")
public class AddAnswerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("list-question");	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		request.setCharacterEncoding("UTF-8");
		// セッションから運用者IDを取得
		HttpSession session = request.getSession();
		OperatorDto operatorDto = (OperatorDto)session.getAttribute("operator");
		int operatorId = operatorDto.getOperatorId();
		
		// フォームから回答文と質問IDを取得
		String answer = request.getParameter("answer");
		int questionId = Integer.parseInt(request.getParameter("questionId"));
		int courseId = Integer.parseInt(request.getParameter("courseId"));
		
		// 遷移先のURLを設定
		String url = "list-question?courseId=" + courseId;
		
		// 入力チェック
		String message = "";
		if ("".equals(answer))  {
			message = "回答を入力してください";
		} else if(answer.length() > 1000) {
			message = "1000字以内に入力してください";
		}
		
		// 入力にエラーがあればリクエストスコープに保持して、質問一覧に遷移
		if (!"".equals(message)) {
			session.setAttribute("message", message);
			response.sendRedirect(url);
			return;
		}
		
		try (Connection con = DataSourceManager.getConnection()) {
			// データベースに保存
			AnswerDao answerDao = new AnswerDao(con);
			answerDao.insert(questionId, answer, operatorId);
			
			// 成功時メッセージをセッションスコープに保持
			session.setAttribute("successMessage", "送信しました");
			
			// 質問一覧にリダイレクト
			response.sendRedirect(url);
		} catch (SQLException | NamingException e) {
			request.getRequestDispatcher("../system-error.jsp").forward(request, response);
		}
	}

}
