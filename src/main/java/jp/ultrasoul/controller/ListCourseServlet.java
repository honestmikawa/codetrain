package jp.ultrasoul.controller;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.ultrasoul.DataSourceManager;
import jp.ultrasoul.dao.CategoryDao;
import jp.ultrasoul.dao.CourseDao;
import jp.ultrasoul.dao.StudyHistoryDao;
import jp.ultrasoul.dto.CategoryDto;
import jp.ultrasoul.dto.CourseDto;
import jp.ultrasoul.dto.StudyHistoryDto;
import jp.ultrasoul.dto.UserDto;

/**
 * Servlet implementation class ListCourseServlet
 */
@WebServlet("/list-course")
public class ListCourseServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private Logger logger = LoggerFactory.getLogger(ListCourseServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		
		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());
		

		
		 try(Connection conn =  DataSourceManager.getConnection()){
			 
			 request.setAttribute("uri", request.getRequestURI());
			 
			 //ログイン状態を取得する
			 UserDto user = (UserDto) session.getAttribute("user");
			 request.setAttribute("user", user);
			 if(user !=	null) {
			 int userId = user.getUserId();
			 
			 //studyhistorylistを取得する
			 StudyHistoryDao StudyHistoryDao = new StudyHistoryDao(conn);
			 List<StudyHistoryDto> studyHistoryList = StudyHistoryDao.selectByUserId(userId);
			 
			 request.setAttribute("studyHistoryList", studyHistoryList);
			 
			 }
			 
			// メッセージをリクエストに保持する
			 request.setAttribute("message", session.getAttribute("message"));
			 session.removeAttribute("message");
			 
			 // category listを取得する
			 CategoryDao CategoryDao = new CategoryDao(conn);
			 List<CategoryDto> categoryList = CategoryDao.selectAll();
			 
			 request.setAttribute("categoryList", categoryList);
			 
			// course listを取得する
			 CourseDao CourseDao = new CourseDao(conn);
			 List<CourseDto> courseList = CourseDao.selectAll();
			 
			 request.setAttribute("courseList", courseList);
			 
			 //list-course.jspに遷移する
			 request.getRequestDispatcher("/WEB-INF/list-course.jsp").forward(request, response);
			 
		 } catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// doGetを呼び出す
		doGet(request, response);
	}

}
