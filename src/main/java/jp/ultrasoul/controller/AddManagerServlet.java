package jp.ultrasoul.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.ultrasoul.DataSourceManager;
import jp.ultrasoul.dao.ManagerDao;
import jp.ultrasoul.dto.ManagerDto;
import jp.ultrasoul.dto.OperatorDto;

/**
 * Servlet implementation class AddManagerServlet
 */
@WebServlet("/operator/add-manager")
public class AddManagerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// セッションを取得する
		HttpSession session = request.getSession(false);

		// URIをリクエストに保持する
		request.setAttribute("uri", request.getRequestURI());

		// エラーメッセージをリクエストに保持する
		request.setAttribute("errorMessage", session.getAttribute("errorMessage"));

		// セッションスコープからエラーメッセージを削除する
		session.removeAttribute("errorMessage");

		// 法人アカウント作成ページへ遷移する
		request.getRequestDispatcher("/WEB-INF/view-manager.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		// セッションを取得する
		HttpSession session = request.getSession(false);

		// URIをリクエストに保持する
		request.setAttribute("uri", request.getRequestURI());

		// ログイン運用者情報を取得する
		OperatorDto operatorDto = new OperatorDto();
		//権限を取得する
		operatorDto.setAuthority(request.getParameter("authority"));

		// フォームのデータを取得する
		
		ManagerDto managerDto = new ManagerDto();
		managerDto.setEmail(request.getParameter("email"));
		managerDto.setPosition(request.getParameter("position"));
		managerDto.setName(request.getParameter("name"));
		managerDto.setBillingAddress(request.getParameter("billing_address"));
		managerDto.setDomain(request.getParameter("domain"));

		// 入力チェックを行う
		// 項目が一つでも空白ならば「すべての項目を入力してください」を保持する
		if ("".equals(managerDto.getEmail()) || "".equals(managerDto.getPosition()) || "".equals(managerDto.getName())
				|| "".equals(managerDto.getBillingAddress()) || "".equals(managerDto.getDomain())) {
			session.setAttribute("errorMessage", "すべての項目を入力してください");

			// AddManagerServletへ遷移する
			response.sendRedirect("add-manager");
			return;
		}

		// メールアドレスが255文字を超えるならば「メールアドレスは255文字以内に入力してください」を保持する
		else if (managerDto.getEmail().length() > 255) {
			session.setAttribute("errorMessage", "メールアドレスは255文字以内に入力してください");

			// AddManagerServletへ遷移する
			response.sendRedirect("add-manager");
			return;
		}

		// 役職が30文字を超えるならば「役職は30文字以内に入力してください」を保持する
		else if (managerDto.getPosition().length() > 30) {
			session.setAttribute("errorMessage", "役職は30文字以内に入力してください");

			// AddManagerServletへ遷移する
			response.sendRedirect("add-manager");
			return;
		}

		// 氏名が30文字を超えるならば「氏名は30文字以内に入力してください」を保持する
		else if (managerDto.getName().length() > 30) {
			session.setAttribute("errorMessage", "氏名は30文字以内に入力してください");

			// AddManagerServletへ遷移する
			response.sendRedirect("add-manager");
			return;
		}

		// 請求先が255文字を超えるならば「請求先は255文字以内に入力してください」を保持する
		else if (managerDto.getBillingAddress().length() > 255) {
			session.setAttribute("errorMessage", "請求先は255文字以内に入力してください");

			// AddManagerServletへ遷移する
			response.sendRedirect("add-manager");
			return;
		}

		// ドメインが255文字を超えるならば「ドメインは255文字以内に入力してください」を保持する
		else if (managerDto.getDomain().length() > 255) {
			session.setAttribute("errorMessage", "ドメインは255文字以内に入力してください");

			// AddManagerServletへ遷移する
			response.sendRedirect("add-manager");
			return;
		}

		// コネクションを追加する

		try (Connection con = DataSourceManager.getConnection()) {
			// 法人情報を追加する
			ManagerDao managerDao = new ManagerDao(con);
			managerDao.insert(managerDto);

			// メッセージ「追加しました」をセッションスコープに保持する
			session.setAttribute("message", "追加しました");

//			// ListUserServletにフォワードする
//			ServletContext context = request.getServletContext();
//			RequestDispatcher rd = context.getRequestDispatcher("/operator/list-manager");
//			rd.forward(request, response);

			response.sendRedirect("list-manager");

		} catch (SQLException | NamingException e) {
			// メールアドレスが重複している場合とドメインが重複している場合
			if (e.getMessage().contains("Duplicate entry")) {
				// エラーメッセージ(メールアドレスまたはドメインがすでに存在しています)を保持する
				// エラーメッセージをセッションスコープに保持する
				session.setAttribute("errorMessage", "メールアドレスまたはドメインが既に存在しています");
				// AddUserServletへ遷移する
				response.sendRedirect("add-manager");
				return;


//				session.setAttribute("errorMessage", "メールアドレス「" + managerDto.getEmail() + "」はすでに存在しています");
//				// AddUserServletへ遷移する
//				response.sendRedirect("add-manager");
//				return;
//			} else if (e.getMessage().contains("Duplicate entry")) {
//				// エラーメッセージ(ドメインがすでに存在しています)を保持する
//				// エラーメッセージをセッションスコープに保持する
//
//				session.setAttribute("errorMessage", "ドメイン「" + managerDto.getDomain() + "」は既に存在します");
//				// AddUserServletへ遷移する
//				response.sendRedirect("add-manager");
//				return;
			
			} else{

				// システムエラー画面に遷移する
				response.sendRedirect("system-error.jsp");
			}

		}
	}
}
