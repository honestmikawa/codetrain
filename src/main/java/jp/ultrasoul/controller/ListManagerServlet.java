package jp.ultrasoul.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.ultrasoul.DataSourceManager;
import jp.ultrasoul.dao.ManagerDao;
import jp.ultrasoul.dto.ManagerDto;
import jp.ultrasoul.dto.OperatorDto;

/**
 * Servlet implementation class ListManagerServlet
 */
@WebServlet("/operator/list-manager")
public class ListManagerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private Logger logger = LoggerFactory.getLogger(ListManagerServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
		HttpSession session = request.getSession();
		
		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());
	
		 try(Connection conn =  DataSourceManager.getConnection()){
			 
			// メッセージをリクエストに保持する
			 request.setAttribute("message", session.getAttribute("message"));
			 session.removeAttribute("message");
			 
//			 //ログイン情報を取得する
//			 request.setAttribute("operator",session.getAttribute("operator"));
			 
			 OperatorDto operatorDto = (OperatorDto)session.getAttribute("operator");
			 String authority = operatorDto.getAuthority();
			 request.setAttribute("authority", authority);
			 
			 // manager listを取得する
			 ManagerDao managerDao = new ManagerDao(conn);
			 List<ManagerDto> managerList = managerDao.selectAll();
			 
			 request.setAttribute("managerList", managerList);
			 
			 
			 
			 //list-manager.jspに遷移する
			 request.getRequestDispatcher("/WEB-INF/list-manager.jsp").forward(request, response);
			 
		 } catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
	
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
