package jp.ultrasoul.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.ultrasoul.DataSourceManager;
import jp.ultrasoul.dao.UserDao;
import jp.ultrasoul.dto.ManagerDto;
import jp.ultrasoul.dto.UserDto;

/**
 * Servlet implementation class AddUserServlet
 */
@WebServlet("/manager/add-user")
public class AddUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Logger logger = LoggerFactory.getLogger(EditUserServlet.class);
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("追加機能");
		// セッションを取得する
		HttpSession session = request.getSession(false);

		// エラーメッセージをリクエストに保持する
		request.setAttribute("errorMessage", session.getAttribute("errorMessage"));
		
		//セッションスコープからエラーメッセージを削除する
		session.removeAttribute("errorMessage");

		// 利用者アカウント作成ページへ遷移する
		request.getRequestDispatcher("/WEB-INF/view-user.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// セッションを取得する
		HttpSession session = request.getSession(false);
		// URIをリクエストに保持する
		request.setAttribute("uri", request.getRequestURI());

		// ログイン管理者情報を取得する
		ManagerDto manager = (ManagerDto) session.getAttribute("manager");

		// フォームのデータを入力する
		request.setCharacterEncoding("UTF-8");
		UserDto userDto = new UserDto();
		userDto.setName(request.getParameter("name"));
		userDto.setEmail(request.getParameter("email"));

		// 取得した管理者情報から管理者IDを取得する
		userDto.setManagerId(manager.getManagerId());

		// 入力チェックを行う
		// メールアドレスまたは氏名が空白ならば「すべての項目を入力してください」を出力
		if ("".equals(userDto.getEmail()) || "".equals(userDto.getName())) {
			session.setAttribute("errorMessage", "すべての項目を入力してください");

			// AddUserServletへ遷移する
			response.sendRedirect("add-user");
			return;
		}

		// メールアドレスが255文字を超えるならば「メールアドレスは255文字以内に入力してください」を保持する
		else if (userDto.getEmail().length() > 255) {
			session.setAttribute("errorMessage", "メールアドレスは255文字以内に入力してください");

			// AddUserServletへ遷移する
			response.sendRedirect("add-user");
			return;
		}

		// 氏名が30文字を超えるならば「氏名は30文字以内に入力してください」を保持
		else if (userDto.getName().length() > 30) {
			session.setAttribute("errorMessage", "氏名は30文字以内に入力してください");
	
			// AddUserServletへ遷移する
			response.sendRedirect("add-user");
			return;
		}
		
		// メールアドレスがすでに存在していた場合
        if (userDto.getEmail().contains("Duplicate entry")) {
        	// エラーメッセージ(メールアドレスがすでに存在しています)を保持する
			// エラーメッセージをセッションスコープに保持する
			
			session.setAttribute("errorMessage","メールアドレス「" + userDto.getEmail() + "」は既に存在します");
			// AddUserServletへ遷移する
			response.sendRedirect("add-user");
			return;
        }

		// コネクションを追加する

		try (Connection con = DataSourceManager.getConnection()) {
			// 利用者情報を追加する
			UserDao userDao = new UserDao(con);
			userDao.insert(userDto);

			// メッセージ「追加しました」をセッションスコープに保持する
			session.setAttribute("Message", "追加しました");

			// ListUserServletにフォワードする
//			ServletContext context = request.getServletContext();
//			RequestDispatcher rd = context.getRequestDispatcher("/manager/list-user");
//			rd.forward(request, response);
			response.sendRedirect("list-user");

		} catch (SQLException | NamingException e) {
			// メールアドレスがすでに存在していた場合
	        if (e.getMessage().contains("Duplicate entry")) {
	        	// エラーメッセージ(メールアドレスがすでに存在しています)を保持する
				// エラーメッセージをセッションスコープに保持する
				
				session.setAttribute("errorMessage","メールアドレス「" + userDto.getEmail() + "」は既に存在します");
				// AddUserServletへ遷移する
				response.sendRedirect("add-user");
				return;
	        } else {

			// システムエラー画面に遷移する
			response.sendRedirect("system-error.jsp");
		}
		}
	}

}
