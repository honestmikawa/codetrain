package jp.ultrasoul.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.ultrasoul.DataSourceManager;
import jp.ultrasoul.dao.CourseDao;
import jp.ultrasoul.dao.StudyHistoryDao;
import jp.ultrasoul.dto.CourseDto;
import jp.ultrasoul.dto.StudyHistoryDto;
import jp.ultrasoul.dto.UserDto;

/**
 * Servlet implementation class TextServlet
 */
@WebServlet("/text-viewer")
public class TextViewerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private Logger logger = LoggerFactory.getLogger(TextViewerServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());
		
		// リクエストパラメータからコースIDを取得
		int courseId = Integer.parseInt(request.getParameter("courseId"));
		
		// セッションから利用者情報を取得
		HttpSession session = request.getSession();
		UserDto user = (UserDto)session.getAttribute("user");
		
		try (Connection con = DataSourceManager.getConnection()) {
			// コースIDに紐づくコース情報を取得
			CourseDao courseDao = new CourseDao(con);
			CourseDto courseDto = courseDao.selectByCourseId(courseId);
			
			// フリーコースを未ログインで見ようとしたらコース一覧に戻す。
			if (!courseDto.isFreeFlg() && user == null) {
				response.sendRedirect("list-course");
				return;
			}
			
			// PDFへのパスをリクエストスコープに保持
			request.setAttribute("pdfPath", courseDto.getPdfPath());
			
			// ログイン時は学習状況を取得する。
			boolean haveHistory = false;
			int lastPage = 1;
			int maxPage = 1;
			if (user != null) {
				// 学習履歴表から利用者IDとコースIDに紐づく学習履歴を取得する
				StudyHistoryDao sHistoryDao = new StudyHistoryDao(con);
				StudyHistoryDto sHistoryDto = sHistoryDao.selectByUserIdAndCourseId(user.getUserId(), courseId);
				
				// まだ学習履歴がない場合は新しく作成する。
				if (sHistoryDto == null) {
					sHistoryDao.insert(user.getUserId(), courseId);
				} else {
					//学習履歴がある場合は最終閲覧ページと最大閲覧ページを設定する。
					lastPage = sHistoryDto.getLastPage();
					maxPage = sHistoryDto.getMaxPage();
				}
				// 学習履歴存在フラグをオンにする
				haveHistory = true;
			}
			
			// 学習履歴をリクエストスコープに保持する
			request.setAttribute("maxPage", maxPage);
			request.setAttribute("lastPage", lastPage);
			request.setAttribute("haveHistory", haveHistory);
			request.setAttribute("courseId", courseId);
			
			// 続きからボタンから遷移してきた場合は続きからフラグを設定する
			if (request.getParameter("isContinue") != null) {
				request.setAttribute("isContinue", true);
			}
			
			// セッションにテキスト表示用の権利を設定
			session.setAttribute("text-ticket", courseId);
			
			// サーバのURLをリクエストスコープに保持
			String serverUrl = request.getScheme() + "://" + request.getServerName() + ":" 
								+ request.getServerPort() + request.getContextPath() + "/text";
			request.setAttribute("serverUrl", serverUrl);
			
			// text-viewer.jspにフォワードする
			request.getRequestDispatcher("WEB-INF/text-viewer.jsp").forward(request, response);;
		} catch (SQLException | NamingException e) {
			logger.error("{} {}", e.getClass(), e.getMessage());
			// システムエラーに遷移する
			request.getRequestDispatcher("system-error.jsp").forward(request, response);
		} 
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
