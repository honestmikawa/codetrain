package jp.ultrasoul.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.ultrasoul.DataSourceManager;
import jp.ultrasoul.dao.UserDao;
import jp.ultrasoul.dto.UserDto;

/**
 * Servlet implementation class UserLoginServlet
 */
@WebServlet("/login")
public class UserLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private Logger logger = LoggerFactory.getLogger(UserLoginServlet.class);
	private String INIT_PASSWORD = "pass";
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());
		response.sendRedirect("list-course");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());
		
		// セッションを取得する
		HttpSession session = request.getSession(true);
		
		// 既に利用者としてログイン済みならコース一覧に遷移する
		if (session.getAttribute("user") != null) {
			response.sendRedirect("/list-course");
		}
		
		//　フォームのデータを取得する
		String loginId = request.getParameter("id");
		String loginPassword = request.getParameter("password");
		String uri = request.getParameter("uri");

		// ログインID、パスワードが未入力の場合
		if ("".equals(loginId) || "".equals(loginPassword)) {
			logger.warn("ログイン失敗 {}", request.getRemoteAddr());

			session.setAttribute("navbarMessage", "メールアドレス、パスワードを入力してください");
			
			// ログイン処理前にページ情報が存在しない場合はコース一覧に遷移する
			response.sendRedirect(uri);
			return;
		}
		
		try (Connection conn = DataSourceManager.getConnection()) {

			// ログイン処理
			UserDao loginDao = new UserDao(conn);
			UserDto userDto = loginDao.findByIdAndPassword(loginId, loginPassword);

			// ログイン失敗時
			String navbarMessage = "";
			if (userDto == null) {
				logger.warn("ログイン失敗 {} mail={} pass={}", request.getRemoteAddr(), loginId, loginPassword);
				navbarMessage =  "メールアドレスまたはパスワードが間違っています";
			} else if ("rest".equals(userDto.getStatus())) {
				navbarMessage = "このアカウントは休止中です";
			}
			
			// エラーメッセージをセッションに保持して元のページへリダイレクト
			if (!"".equals(navbarMessage)) {
				session.setAttribute("navbarMessage", navbarMessage);
				response.sendRedirect(uri);
				return;
			}
			
			// 利用者情報をセッションに保持
			session.setAttribute("user", userDto);
			session.removeAttribute("navbarMessage");
			
			// パスワードが初期のままならパスワード変更画面に遷移する
			if (INIT_PASSWORD.equals(loginPassword)) {
				session.setAttribute("message", "初期パスワードです。パスワードを変更してください");
				response.sendRedirect("edit-password");
			} else {
				// ログイン処理前にページ情報が存在しない場合はコース一覧に遷移する
				response.sendRedirect(uri);
			}	
		} catch (SQLException | NamingException e) {
			logger.error("{} {}", e.getClass(), e.getMessage());
			
			// システムエラーに遷移する
			request.getRequestDispatcher("system-error.jsp").forward(request, response);
		}
	}
}
