package jp.ultrasoul.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.ultrasoul.DataSourceManager;
import jp.ultrasoul.dao.AnswerDao;
import jp.ultrasoul.dao.QuestionDao;
import jp.ultrasoul.dto.AnswerDto;
import jp.ultrasoul.dto.QuestionDto;

/**
 * Servlet implementation class ListQuestionServlet
 */
@WebServlet("/list-question")
public class ListQuestionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private Logger logger = LoggerFactory.getLogger(ListQuestionServlet.class);


	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
		HttpSession session = request.getSession();
		
		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());
		
		//コースIDを取得
		int courseId = Integer.parseInt(request.getParameter("courseId"));
		//コースIDをリクエストパラメータに入れる
		request.setAttribute("courseId", courseId);
		
		// URIを保持
		request.setAttribute("uri", request.getRequestURI() + "?courseId=" + courseId);
		
		try(Connection con =  DataSourceManager.getConnection()){
			
			
			// メッセージをリクエストに保持する
			 request.setAttribute("message", session.getAttribute("message"));
			 session.removeAttribute("message");
			 
			// 送信成功メッセージをリクエストに保持する
			 request.setAttribute("successMessage", session.getAttribute("successMessage"));
			 session.removeAttribute("successMessage");
			 
			 // 質問情報リストを取得
			 QuestionDao QuestionDao = new QuestionDao(con);
			 List<QuestionDto> questionList = QuestionDao.selectByCourseId(courseId);
			 
			 request.setAttribute("questionList", questionList);
			 
			// 回答情報リストを取得
			 AnswerDao answerDao = new AnswerDao(con);
			 List<AnswerDto> answerList = answerDao.selectByCourseId(courseId);
			 
			 request.setAttribute("answerList", answerList);
			 
			//list-question.jspに遷移する
			 request.getRequestDispatcher("/WEB-INF/list-question.jsp").forward(request, response);
			
			 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
