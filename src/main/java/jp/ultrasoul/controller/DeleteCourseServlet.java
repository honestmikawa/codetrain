package jp.ultrasoul.controller;

import java.io.File;
import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.ultrasoul.DataSourceManager;
import jp.ultrasoul.dao.CourseDao;
import jp.ultrasoul.dto.CourseDto;

/**
 * Servlet implementation class DeleteCourseServlet
 */
@WebServlet("/operator/delete-course")
@MultipartConfig(maxFileSize=1048576)
public class DeleteCourseServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private Logger logger = LoggerFactory.getLogger(DeleteCourseServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//ListCourseServletにリダイレクトする
		response.sendRedirect("/CodeTrain/operator/list-course-operator");

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//フォームのデータを取得する
		request.setCharacterEncoding("UTF-8");
		CourseDto courseDto = new CourseDto();
		//コースID
		int courseId = Integer.parseInt(request.getParameter("courseId"));
		
		// データベースへ接続する
		try (Connection conn = DataSourceManager.getConnection()) {

			CourseDao courseDao = new CourseDao(conn);
			
			//PDFのパスに紐づくファイルをサーバから削除する
			String fName = "course" + courseId + ".pdf";
			String fDir = getServletContext().getRealPath("pdf");
			
			File file = new File(fDir + "/" + fName);
			file.delete();
			
			//コースIDに紐づくコース情報を学習コーステーブルから削除する
			courseDao.deleteByCourseId(courseId);
						
		} catch (SQLException | NamingException e) {
			// システムエラー画面に転送する
			request.getRequestDispatcher("/system-error.jsp").forward(request, response);
		}
		
		//セッションを取得する
		HttpSession session = request.getSession();
		
		//メッセージ(削除しました)をセッションスコープに保持する
		session.setAttribute("message", "削除しました");
		
		//カテゴリIDをパラメータに付与し、ListCourseServletにリダイレクトする
		response.sendRedirect("/CodeTrain/operator/list-course-operator?courseId=" + request.getParameter("categoryId"));
	}

}
