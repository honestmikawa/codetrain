package jp.ultrasoul.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.ultrasoul.DataSourceManager;
import jp.ultrasoul.dao.CourseDao;
import jp.ultrasoul.dao.StudyHistoryDao;
import jp.ultrasoul.dto.CourseDto;
import jp.ultrasoul.dto.StudyHistoryDto;
import jp.ultrasoul.dto.UserDto;

/**
 * Servlet implementation class TextServlet
 */
@WebServlet("/operator/text-viewer")
public class TextViewerOperatorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private Logger logger = LoggerFactory.getLogger(TextViewerServlet.class);

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());
		
		// リクエストパラメータからコースIDを取得
		int courseId = Integer.parseInt(request.getParameter("courseId"));
		
		// リクエストスコープにコースIDを保持
		request.setAttribute("courseId", courseId);
		
		// セッションを取得
		HttpSession session = request.getSession();

		try (Connection con = DataSourceManager.getConnection()) {
			// コースIDに紐づくコース情報を取得
			CourseDao courseDao = new CourseDao(con);
			CourseDto courseDto = courseDao.selectByCourseId(courseId);
			
			// PDFへのパスをリクエストスコープに保持
			request.setAttribute("pdfPath", courseDto.getPdfPath());

			// セッションにテキスト表示用の権利を設定
			session.setAttribute("text-ticket", courseId);
			
			// サーバのURLをリクエストスコープに保持
			String serverUrl = request.getScheme() + "://" + request.getServerName() + ":" 
								+ request.getServerPort() + request.getContextPath() + "/text";
			request.setAttribute("serverUrl", serverUrl);
			
			// text-viewer.jspにフォワードする
			request.getRequestDispatcher("/WEB-INF/text-viewer-operator.jsp").forward(request, response);;
		} catch (SQLException | NamingException e) {
			logger.error("{} {}", e.getClass(), e.getMessage());
			// システムエラーに遷移する
			request.getRequestDispatcher("../system-error.jsp").forward(request, response);
		} 
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
