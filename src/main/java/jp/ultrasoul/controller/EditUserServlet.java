package jp.ultrasoul.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
//import javax.servlet.RequestDispatcher;
//import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import jp.ultrasoul.DataSourceManager;
import jp.ultrasoul.dao.UserDao;
import jp.ultrasoul.dto.ManagerDto;
import jp.ultrasoul.dto.UserDto;

/**
 * Servlet implementation class EditUserServlet
 */
@WebServlet("/manager/edit-user")
public class EditUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Logger logger = LoggerFactory.getLogger(EditUserServlet.class);
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("更新");
		//セッションを取得する
		HttpSession session = request.getSession(false);
		
		//メッセージをリクエストスコープに保持する
		request.setAttribute("Message", session.getAttribute("Message"));
		
//		//セッションスコープからメッセージを削除する
//		session.removeAttribute("Message");
//		
//		// エラーメッセージをリクエストに保持する
//		request.setAttribute("errorMessage", session.getAttribute("errorMessage"));
//		
//		//セッションスコープからエラーメッセージを削除する
//		session.removeAttribute("errorMessage");

		
		//利用者一覧ページに遷移する
		request.getRequestDispatcher("/manager/list-user").forward(request, response);
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//セッションを取得する
		HttpSession session = request.getSession(false);
		
		// URIをリクエストに保持する
		request.setAttribute("uri", request.getRequestURI());

		// ログイン管理者情報を取得する
		ManagerDto manager = (ManagerDto) session.getAttribute("manager");
		
		//更新する利用者情報を取得する
		//UserDto user = (UserDto) session.getAttribute("user");
		request.setCharacterEncoding("UTF-8");
		UserDto userDto = new UserDto();
		userDto.setStatus(request.getParameter("status"));
		userDto.setPauseReason(request.getParameter("pauseReason"));
		userDto.setUserId(Integer.parseInt(request.getParameter("userId")));
		// 取得した管理者情報から管理者IDを取得する
		userDto.setManagerId(manager.getManagerId());
		
		//入力チェックを行う
		//ステータスの状態がactiveならば休止理由の入力チェックを行う
//		if (userDto.getStatus().equals("active")) {
//		// 休止理由が空白ならば「休止理由を入力してください」を出力
//		if ("".equals(userDto.getPauseReason())) {
//			session.setAttribute("errorMessage", "休止理由を入力してください");
//
//			// EditUserServletへ遷移する
//			response.sendRedirect("edit-user");
//			return;
//		}
//		}

		//コネクションを開始する
		try (Connection conn = DataSourceManager.getConnection()){			
				UserDao userDao = new UserDao(conn);
				userDao.update(userDto);
			//メッセージ「変更しました」を表示する
			session.setAttribute("Message", "変更しました");
			
			response.sendRedirect("edit-user");
			
//			ServletContext context = request.getServletContext();
//			RequestDispatcher rd = context.getRequestDispatcher("/manager/list-user");
//			rd.forward(request, response);

		} catch (SQLException | NamingException e) {
			logger.info("start:{} {}", Thread.currentThread().getStackTrace()[1].getMethodName(), request.getRemoteAddr());
			// システムエラー画面に遷移する
			response.sendRedirect("system-error.jsp");
		}




	}

}
