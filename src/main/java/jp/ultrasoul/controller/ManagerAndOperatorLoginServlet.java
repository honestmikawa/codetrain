package jp.ultrasoul.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.ultrasoul.DataSourceManager;
import jp.ultrasoul.dao.ManagerDao;
import jp.ultrasoul.dao.OperatorDao;
import jp.ultrasoul.dto.ManagerDto;
import jp.ultrasoul.dto.OperatorDto;

/**
 * Servlet implementation class ManagerAndOperatorLoginServlet
 */
@WebServlet("/mo-login")
public class ManagerAndOperatorLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private Logger logger = LoggerFactory.getLogger(ManagerAndOperatorLoginServlet.class);
	private String INIT_PASSWORD = "password";
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());
		
		// セッションを取得
		HttpSession session = request.getSession(true);
		
		// 管理者としてログイン済みなら利用者一覧へ遷移
		if(session.getAttribute("manager") != null) {
			response.sendRedirect("manager/list-user");
			return;
		}
		
		// 運用者としてログイン済みなら管理者一覧へ遷移
		if (session.getAttribute("operator") != null) {
			response.sendRedirect("operator/list-manager");
			return;
		}

		// セッションにメッセージがあればリクエストスコープに保持して、セッションから破棄
		if (session.getAttribute("message") != null) {
			request.setAttribute("message", session.getAttribute("message"));
			session.removeAttribute("message");
		}
		
		// ログインページのjspにフォワード
		request.getRequestDispatcher("WEB-INF/manager-operator-login.jsp").forward(request, response);;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		logger.info("start:{}", Thread.currentThread().getStackTrace()[1].getMethodName());
		
		// セッションを取得する
		HttpSession session = request.getSession(true);
		
		// 既に管理者としてログイン済みなら利用者一覧に遷移する
		if (session.getAttribute("manager") != null) {
			response.sendRedirect("manager/list-user");
			return;
		}
		
		// 既に運用者としてログイン済みなら管理者一覧に遷移する
		if (session.getAttribute("operator") != null) {
			response.sendRedirect("operator/list-manager");
			return;
		}
		
		//　フォームのデータを取得する
		String loginId = request.getParameter("id");
		String loginPassword = request.getParameter("password");
		
		// ログインID、パスワードが未入力の場合
		if ("".equals(loginId) || "".equals(loginPassword)) {
			logger.warn("ログイン失敗 {}", request.getRemoteAddr());

			session.setAttribute("message", "メールアドレス、パスワードを入力してください");
			
			// ログインページに遷移する
			response.sendRedirect("mo-login");
			return;
		}
		
		try (Connection conn = DataSourceManager.getConnection()) {

			// 運用者ログイン処理
			OperatorDao operatorDao = new OperatorDao(conn);
			OperatorDto operatorDto = operatorDao.findByIdAndPassword(loginId, loginPassword);
			
			// 運用者としてログイン成功時
			if (operatorDto != null) {
				// 管理者情報をセッションに保持
				session.setAttribute("operator", operatorDto);
				
				// 管理者一覧に遷移
				response.sendRedirect("operator/list-manager");
				return;
			} 
			
			// 運用者としてログインできなければ管理者ログイン処理
			ManagerDao managerDao = new ManagerDao(conn);
			ManagerDto managerDto = managerDao.findByIdAndPassword(loginId, loginPassword);
			
			// ログイン失敗時
			if (managerDto == null) {
				// エラーメッセージをセッションに保持してログインページに遷移
				logger.warn("ログイン失敗 {} mail={} pass={}", request.getRemoteAddr(), loginId, loginPassword);
				session.removeAttribute("message");
				session.setAttribute("message", "メールアドレスまたはパスワードが間違っています");
				response.sendRedirect("mo-login");
				return;
			}
			
			session.setAttribute("manager", managerDto);
			
			// 初回ログイン時はパスワード変更画面に遷移
			if(INIT_PASSWORD.equals(loginPassword)) {
				session.setAttribute("message", "初期パスワードです。パスワードを変更してください");
				response.sendRedirect("manager/edit-password");
			} else {
				// 初回ログインでなければ管理者一覧に遷移
				response.sendRedirect("manager/list-user");	
			}

		} catch (SQLException | NamingException e) {
			logger.error("{} {}", e.getClass(), e.getMessage());
			// システムエラーに遷移する
			request.getRequestDispatcher("system-error.jsp").forward(request, response);
		}
	}
}
