package jp.ultrasoul.dto;

public class BillingDto {

	private int managerId;
	
	private int time;
	
	private int userNumber;
	
	private int pauserNumber;
	
	private int billingAmount;
	
	private String name;
	
	private String billingAddress;

	public String getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(String billingAddress) {
		this.billingAddress = billingAddress;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getManagerId() {
		return managerId;
	}

	public void setManagerId(int managerId) {
		this.managerId = managerId;
	}

	public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
	}

	public int getUserNumber() {
		return userNumber;
	}

	public void setUserNumber(int userNumber) {
		this.userNumber = userNumber;
	}

	public int getPauserNumber() {
		return pauserNumber;
	}

	public void setPauserNumber(int pauserNumber) {
		this.pauserNumber = pauserNumber;
	}

	public int getBillingAmount() {
		return billingAmount;
	}

	public void setBillingAmount(int billingAmount) {
		this.billingAmount = billingAmount;
	}
	
}
