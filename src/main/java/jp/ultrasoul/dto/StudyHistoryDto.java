package jp.ultrasoul.dto;

import java.sql.Timestamp;

public class StudyHistoryDto {
	
	private int userId;
	
	private int courseId;
	
	private Timestamp lastStudyTime;
	
	private int lastPage;
	
	private int maxPage;
	
	private int totalPages;
	
	private String name;
	
	private String categoryName;
	
	private String courseName;

	public String getCategoryName() {
		return categoryName;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getCourseId() {
		return courseId;
	}

	public void setCourseId(int courseId) {
		this.courseId = courseId;
	}

	public Timestamp getLastStudyTime() {
		return lastStudyTime;
	}

	public void setLastStudyTime(Timestamp lastStudyTime) {
		this.lastStudyTime = lastStudyTime;
	}

	public int getLastPage() {
		return lastPage;
	}

	public void setLastPage(int lastPage) {
		this.lastPage = lastPage;
	}

	public int getMaxPage() {
		return maxPage;
	}

	public void setMaxPage(int maxPage) {
		this.maxPage = maxPage;
	}

	
}

