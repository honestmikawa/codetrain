package jp.ultrasoul.dto;

import java.sql.Timestamp;

public class AnswerDto {
	
	private int answerId;
	
	private int questionId;
	
	private String answer;
	
	private Timestamp time;
	
	private int operatorId;
	
	private String AnswererName;

	public String getAnswererName() {
		return AnswererName;
	}

	public void setAnswererName(String answererName) {
		AnswererName = answererName;
	}

	public int getAnswerId() {
		return answerId;
	}

	public void setAnswerId(int answerId) {
		this.answerId = answerId;
	}

	public int getQuestionId() {
		return questionId;
	}

	public void setQuestionId(int questionId) {
		this.questionId = questionId;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public Timestamp getTime() {
		return time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	public int getOperatorId() {
		return operatorId;
	}

	public void setOperatorId(int operatorId) {
		this.operatorId = operatorId;
	}
	
}
