package jp.ultrasoul;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet Filter implementation class CheckManagerLoginFilter
 */
@WebFilter("/manager/*")
public class CheckManagerLoginFilter implements Filter {
	
	@Override
	public void init(FilterConfig filterConfig){
	}

	@Override
	public void destroy(){
	}
	
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpSession session = ((HttpServletRequest)request).getSession(false);
		if(session == null || session.getAttribute("manager") == null) {
			((HttpServletResponse)response).sendRedirect("../mo-login");
		} else {
			chain.doFilter(request, response);			
		}
	}
}
