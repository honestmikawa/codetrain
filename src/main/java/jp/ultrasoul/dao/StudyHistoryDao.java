package jp.ultrasoul.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jp.ultrasoul.dto.StudyHistoryDto;
import jp.ultrasoul.dto.UserDto;


public class StudyHistoryDao {
	
	/** コネクション */
	protected Connection con;
	
	public StudyHistoryDao(Connection con) {
		this.con = con;
	}
	
public List<StudyHistoryDto> selectByUserId(int userId) throws SQLException{
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append("   select");
		sb.append("          s.USER_ID");
		sb.append("         ,s.COURSE_ID");
		sb.append("         ,LAST_STUDY_TIME");
		sb.append("         ,MAX_PAGE");
		sb.append("         ,TOTAL_PAGES");
		sb.append("			,NAME");
		sb.append("			,CATEGORY_NAME");
		sb.append("			,COURSE_NAME");
		sb.append("     from STUDY_HISTORY s");
		sb.append(" inner join COURSE co");
		sb.append("         on s.COURSE_ID = co.COURSE_ID");
		sb.append("			join USER u");
		sb.append("			on s.user_id = u.user_id");
		sb.append("			join CATEGORY ca");
		sb.append("		on co.CATEGORY_ID = ca.CATEGORY_ID");
		sb.append("  where s.USER_ID = ?");
		sb.append("			order by LAST_STUDY_TIME desc");
		
		List<StudyHistoryDto> list =  new ArrayList<>();
		
		try(PreparedStatement ps = con.prepareStatement(sb.toString())){
			
			ps.setInt(1, userId);
			
			//sql文の実行
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				StudyHistoryDto dto = new StudyHistoryDto();
				dto.setUserId(rs.getInt("s.USER_ID"));
				dto.setCourseId(rs.getInt("s.COURSE_ID"));
				dto.setLastStudyTime(rs.getTimestamp("LAST_STUDY_TIME"));
				dto.setMaxPage(rs.getInt("MAX_PAGE"));
				dto.setTotalPages(rs.getInt("TOTAL_PAGES"));
				dto.setName(rs.getString("NAME"));
				dto.setCategoryName(rs.getString("CATEGORY_NAME"));
				dto.setCourseName(rs.getString("COURSE_NAME"));
				list.add(dto);
			}
		}
		return list;	
}
	
	
	
	
	
	public StudyHistoryDto selectByUserIdAndCourseId(int userId, int courseId) throws SQLException{
		
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append("   select");
		sb.append("          USER_ID");
		sb.append("         ,COURSE_ID");
		sb.append("         ,LAST_STUDY_TIME");
		sb.append("         ,LAST_PAGE");
		sb.append("         ,MAX_PAGE");
		sb.append("   from STUDY_HISTORY");
		sb.append("   where USER_ID = ? and COURSE_ID = ?");
		sb.append("		order by LAST_STUDY_TIME desc");
		
		try(PreparedStatement ps = con.prepareStatement(sb.toString())){
			ps.setInt(1, userId);
			ps.setInt(2, courseId);
			//sql文の実行
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				StudyHistoryDto dto = new StudyHistoryDto();
				dto.setUserId(rs.getInt("USER_ID"));
				dto.setCourseId(rs.getInt("COURSE_ID"));
				dto.setLastStudyTime(rs.getTimestamp("LAST_STUDY_TIME"));
				dto.setLastPage(rs.getInt("LAST_PAGE"));
				dto.setMaxPage(rs.getInt("MAX_PAGE"));
				return dto;
			}
		}
		return null;
	}
	
	public int insert(int userId, int courseId) throws SQLException {
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append("   insert");
		sb.append("   into STUDY_HISTORY");
		sb.append("         (USER_ID");
		sb.append("         ,COURSE_ID");
		sb.append("         ,LAST_PAGE");
		sb.append("         ,MAX_PAGE)");
		sb.append("   values (?, ?, ?, ?)");
		
		try(PreparedStatement ps = con.prepareStatement(sb.toString())) {
			ps.setInt(1, userId);
			ps.setInt(2, courseId);
			ps.setInt(3, 0);
			ps.setInt(4, 0);
			//sql文の実行
			return ps.executeUpdate();
		}
	}
	
	public int update(StudyHistoryDto dto) throws SQLException {
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append("   update");
		sb.append("         STUDY_HISTORY");
		sb.append("   set");
		sb.append("      	LAST_PAGE = ?");
		sb.append("      	,MAX_PAGE = ?");
		sb.append("   where USER_ID = ? and COURSE_ID = ?");
		
		try(PreparedStatement ps = con.prepareStatement(sb.toString())) {
			ps.setInt(1, dto.getLastPage());
			ps.setInt(2, dto.getMaxPage());
			ps.setInt(3, dto.getUserId());
			ps.setInt(4, dto.getCourseId());
			//sql文の実行
			return ps.executeUpdate();
		}
	}
	
	public void delete(StudyHistoryDto dto) throws SQLException {
		//SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" delete from STUDY_HISTORY");
		sb.append("       where USER_ID = ?");
		
		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = con.prepareStatement(sb.toString())) {
        	// プレースホルダーに値をセットする
			ps.setInt(1, dto.getUserId());
			
            // SQLを実行する
			ps.executeUpdate();
		}

	}

}
