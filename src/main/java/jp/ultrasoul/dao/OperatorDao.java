package jp.ultrasoul.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import jp.ultrasoul.dto.OperatorDto;

public class OperatorDao {

	/** コネクション */
	protected Connection con;

	public OperatorDao(Connection con) {
		this.con = con;
	}


	public OperatorDto findByIdAndPassword(String id, String password) throws SQLException {
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append("        OPERATOR_ID");
		sb.append("       ,EMAIL");
		sb.append("       ,NAME");
		sb.append("       ,AUTHORITY");
		sb.append("   from OPERATOR");
		sb.append("  where EMAIL = ?");
		sb.append("    and PASSWORD = sha2(?, 256)");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = con.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setString(1, id);
			ps.setString(2, password);

			// SQLを実行する
			ResultSet rs = ps.executeQuery();

			// 結果をDTOに詰める
			if (rs.next()) {
				OperatorDto operator = new OperatorDto();
				operator.setOperatorId(rs.getInt("OPERATOR_ID"));
				operator.setEmail(rs.getString("EMAIL"));
				operator.setName(rs.getString("NAME"));
				operator.setAuthority(rs.getString("AUTHORITY"));
				return operator;
			}
			// 該当するデータがない場合はnullを返却する
			return null;
		}
	}
	
	public List<OperatorDto> selectAll() throws Exception{

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append("   select");
		sb.append("          OPERATOR_ID");
		sb.append("         ,EMAIL");
		sb.append("         ,PASSWORD");
		sb.append("         ,NAME");
		sb.append("         ,AUTHORITY");
		sb.append("     from OPERATOR");

		List<OperatorDto> list =  new ArrayList<>();

		try(Statement st = con.createStatement()){

			//sql文の実行
			ResultSet rs = st.executeQuery(sb.toString());
			while(rs.next()) {
				OperatorDto dto = new OperatorDto();
				dto.setOperatorId(rs.getInt("OPERATOR_ID"));
				dto.setEmail(rs.getString("EMAIL"));
				dto.setPassword(rs.getString("PASSWORD"));
				dto.setName(rs.getString("NAME"));
				dto.setAuthority(rs.getString("AUTHORITY"));
				list.add(dto);
			}
			return list;
		}
	}

	public int updatePassword(int operatorId, String currentPassword, String newPassword) throws SQLException {
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" update");
		sb.append("        OPERATOR");
		sb.append("    set");
		sb.append("       PASSWORD = sha2(?, 256)");
		sb.append("  where OPERATOR_ID = ? and PASSWORD = sha2(?, 256)");
		
		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = con.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setString(1, newPassword);
			ps.setInt(2, operatorId);
			ps.setString(3, currentPassword);

			// SQLを実行する
			return ps.executeUpdate();
		}
	}
	
	/**
	 * 運営者IDに紐づく氏名、権限を更新する
	 * @param operatorId　運営者ID
	 * @return　　　　　　更新結果
	 * @throws SQLException　SQL例外
	 */
	public int update(OperatorDto dto) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" update");
		sb.append("        OPERATOR");
		sb.append("    set");
		sb.append("        NAME = ?");
		sb.append("       ,AUTHORITY = ?");
		sb.append("  where OPERATOR_ID = ?");
	
    	// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = con.prepareStatement(sb.toString())) {
			
        	// プレースホルダーに値をセットする
			ps.setString(1, dto.getName());
			ps.setString(2, dto.getAuthority());
			ps.setInt(3, dto.getOperatorId());

            // SQLを実行する
			return ps.executeUpdate();
		}
	}
}
