package jp.ultrasoul.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import jp.ultrasoul.dto.ManagerDto;


public class ManagerDao {
	
	/** コネクション */
	protected Connection con;
	
	public ManagerDao(Connection con) {
		this.con = con;
	}
	
	public void insert(ManagerDto dto) throws SQLException {
		//SQL文の作成
		StringBuffer sb = new StringBuffer();
		sb.append(" insert into MANAGER");
		sb.append("                   (");
		sb.append("                     EMAIL");
		sb.append("                    ,PASSWORD");
		sb.append("                    ,POSITION");
		sb.append("                    ,NAME");
		sb.append("                    ,BILLING_ADDRESS");
		sb.append("                    ,DOMAIN");
		sb.append("                   )");
		sb.append("              values");
		sb.append("                   (");
		sb.append("                     ?");
		sb.append("                    ,sha2('password', 256)");
		sb.append("                    ,?");
		sb.append("                    ,?");
		sb.append("                    ,?");
		sb.append("                    ,?");
		sb.append("                   )");

		//ステートメントオブジェクトを作成する
		try (PreparedStatement ps = con.prepareStatement(sb.toString())) {
			//プレースホルダに値をセットする
			ps.setString(1, dto.getEmail());
			ps.setString(2, dto.getPosition());
			ps.setString(3, dto.getName());
			ps.setString(4, dto.getBillingAddress());
			ps.setString(5, dto.getDomain());
			
			//SQLを実行する
			ps.executeUpdate();
		}
	}


   public ManagerDto findByIdAndPassword(String id, String password) throws SQLException {

    	// SQL文を作成する
    	StringBuffer sb = new StringBuffer();
    	sb.append(" select");
    	sb.append("        MANAGER_ID");
    	sb.append("       ,EMAIL");
    	sb.append("       ,NAME");
    	sb.append("       ,POSITION");
    	sb.append("       ,BILLING_ADDRESS");
    	sb.append("       ,DOMAIN");
    	sb.append("   from MANAGER");
    	sb.append("  where EMAIL = ?");
    	sb.append("    and PASSWORD = sha2(?, 256)");

    	// ステートメントオブジェクトを作成する
        try (PreparedStatement ps = con.prepareStatement(sb.toString())) {
        	// プレースホルダーに値をセットする
            ps.setString(1, id);
            ps.setString(2, password);
            
            // SQLを実行する
            ResultSet rs = ps.executeQuery();
            
            // 結果をDTOに詰める
            if (rs.next()) {
                ManagerDto manager = new ManagerDto();
                manager.setManagerId(rs.getInt("MANAGER_ID"));
                manager.setEmail(rs.getString("EMAIL"));
                manager.setName(rs.getString("NAME"));
                manager.setPosition(rs.getString("POSITION"));
                manager.setBillingAddress(rs.getString("BILLING_ADDRESS"));
                manager.setDomain(rs.getString("DOMAIN"));
                return manager;
            }
            // 該当するデータがない場合はnullを返却する
        	return null;
        }
   }
   
	public int updatePassword(int managerId, String currentPassword, String newPassword) throws SQLException {
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" update");
		sb.append("        MANAGER");
		sb.append("    set");
		sb.append("       PASSWORD = sha2(?, 256)");
		sb.append("  where MANAGER_ID = ? and PASSWORD = sha2(?, 256)");
		
		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = con.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setString(1, newPassword);
			ps.setInt(2, managerId);
			ps.setString(3, currentPassword);

			// SQLを実行する
			return ps.executeUpdate();
		}
	}

   public List<ManagerDto> selectAll() throws SQLException{
		
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append("   select");
		sb.append("          MANAGER_ID");
		sb.append("         ,EMAIL");
		sb.append("         ,PASSWORD");
		sb.append("         ,POSITION");
		sb.append("         ,NAME");
		sb.append("         ,BILLING_ADDRESS");
		sb.append("         ,DOMAIN");
		sb.append("     from MANAGER");
		
		List<ManagerDto> list =  new ArrayList<>();
		
		try(Statement st = con.createStatement()){
			
			//sql文の実行
			ResultSet rs = st.executeQuery(sb.toString());
			while(rs.next()) {
				ManagerDto dto = new ManagerDto();
				dto.setManagerId(rs.getInt("MANAGER_ID"));
				dto.setEmail(rs.getString("EMAIL"));
				dto.setPassword(rs.getString("PASSWORD"));
				dto.setPosition(rs.getString("POSITION"));
				dto.setName(rs.getString("NAME"));
				dto.setBillingAddress(rs.getString("BILLING_ADDRESS"));
				dto.setDomain(rs.getString("DOMAIN"));
				list.add(dto);
			}
			return list;
		}
   }
}
