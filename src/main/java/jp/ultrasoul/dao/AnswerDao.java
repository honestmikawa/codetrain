package jp.ultrasoul.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jp.ultrasoul.dto.AnswerDto;

public class AnswerDao {
	
	/** コネクション */
	protected Connection con;
	
	public AnswerDao(Connection con) {
		this.con = con;
	}
	
	public List<AnswerDto> selectByCourseId(int courseId) throws SQLException {
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append("   select");
		sb.append("          ANSWER_ID");
		sb.append("         ,QUESTION_ID");
		sb.append("         ,ANSWER");
		sb.append("         ,TIME");
		sb.append("         ,NAME");
		sb.append("     from ANSWER");
		sb.append("     inner join OPERATOR");
		sb.append("     on ANSWER.OPERATOR_ID = OPERATOR.OPERATOR_ID");
		sb.append("     where QUESTION_ID IN (");
		sb.append("    select");
		sb.append("     	 QUESTION_ID");
		sb.append("    from QUESTION");
		sb.append("    where COURSE_ID = ?)");

		try (PreparedStatement ps = con.prepareStatement(sb.toString())) {
			ps.setInt(1, courseId);
			
			List<AnswerDto> list = new ArrayList<>();
			// SQL文を実行
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				AnswerDto dto = new AnswerDto();
				dto.setQuestionId(rs.getInt("QUESTION_ID"));
				dto.setAnswerId(rs.getInt("ANSWER_ID"));
				dto.setAnswer(rs.getString("ANSWER"));
				dto.setTime(rs.getTimestamp("TIME"));
				dto.setAnswererName(rs.getString("NAME"));
				list.add(dto);
			}
			return list;
		}
	}
	
	public int insert(int questionId, String answer, int operatorId) throws SQLException {
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append("   insert into");
		sb.append("          ANSWER");
		sb.append("         (QUESTION_ID");
		sb.append("         ,ANSWER");
		sb.append("         ,OPERATOR_ID)");
		sb.append("   values (?, ?, ?);");
		
		// クエリを実行
		try (PreparedStatement ps = con.prepareStatement(sb.toString())) {
			ps.setInt(1, questionId);
			ps.setString(2, answer);
			ps.setInt(3, operatorId);
			
			return ps.executeUpdate();	
		} 
	}

}
