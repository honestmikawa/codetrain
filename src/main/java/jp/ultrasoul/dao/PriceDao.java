package jp.ultrasoul.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jp.ultrasoul.dto.PriceDto;

public class PriceDao {

	/** コネクション */
	protected Connection con;
	
	public PriceDao(Connection con) {
		this.con = con;
	}
	public List<PriceDto> selectByPriceId(int priceId) throws SQLException {
		
		StringBuffer sb = new StringBuffer();
		sb.append("select");
		sb.append("		 PRICE_ID");
		sb.append("		,PRICE");
		sb.append(" from PRICE");
		sb.append(" where PRICE_ID = ?");
		
		List<PriceDto> list = new ArrayList<>();
		
		try(PreparedStatement ps = con.prepareStatement(sb.toString())) {
			
			ps.setInt(1, priceId);
			
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				PriceDto dto = new PriceDto();
				dto.setPriceId(rs.getInt("PRICE_ID"));
				dto.setPrice(rs.getInt("PRICE"));
				list.add(dto);
			}
		}
		return list;
	}
}
