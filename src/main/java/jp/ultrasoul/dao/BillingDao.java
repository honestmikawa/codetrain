package jp.ultrasoul.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jp.ultrasoul.dto.BillingDto;

public class BillingDao {

	/** コネクション */
	protected Connection con;

	public BillingDao(Connection con) {
		this.con = con;
	}

	public List<BillingDto> selectByManagerId(int managerId) throws SQLException {

		StringBuffer sb = new StringBuffer();
		sb.append("select");
		sb.append("		 MANAGER_ID");
		sb.append("		,TIME");
		sb.append("		,USER_NUMBER");
		sb.append("		,PAUSER_NUMBER");
		sb.append("		,BILLING_AMOUNT");
		sb.append("	from BILLING");
		sb.append(" where MANAGER_ID = ?");
		sb.append(" order by TIME desc ");

		List<BillingDto> list = new ArrayList<>();

		try (PreparedStatement ps = con.prepareStatement(sb.toString())) {

			ps.setInt(1, managerId);

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				BillingDto dto = new BillingDto();
				dto.setManagerId(rs.getInt("MANAGER_ID"));
				dto.setTime(rs.getInt("TIME"));
				dto.setUserNumber(rs.getInt("USER_NUMBER"));
				dto.setPauserNumber(rs.getInt("PAUSER_NUMBER"));
				dto.setBillingAmount(rs.getInt("BILLING_AMOUNT"));
				list.add(dto);
			}
		}
		return list;
	}

	public List<BillingDto> selectByManagerIdAndTime(int managerId,int time) throws SQLException {

		StringBuffer sb = new StringBuffer();
		sb.append("select");
		sb.append("		 BILLING.MANAGER_ID");
		sb.append("		,TIME");
		sb.append("		,USER_NUMBER");
		sb.append("		,PAUSER_NUMBER");
		sb.append("		,BILLING_AMOUNT");
		sb.append("		,NAME");
		sb.append("		, BILLING_ADDRESS");
		sb.append("	from BILLING");
		sb.append("	inner join MANAGER");
		sb.append(" on BILLING.MANAGER_ID = MANAGER.MANAGER_ID");
		sb.append(" where BILLING.MANAGER_ID = ?");
		sb.append("		 and TIME = ?");
		sb.append(" order by TIME desc ");

		List<BillingDto> list = new ArrayList<>();

		try (PreparedStatement ps = con.prepareStatement(sb.toString())) {

			ps.setInt(1, managerId);
			ps.setInt(2, time);

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				BillingDto dto = new BillingDto();
				dto.setManagerId(rs.getInt("MANAGER_ID"));
				dto.setTime(rs.getInt("TIME"));
				dto.setUserNumber(rs.getInt("USER_NUMBER"));
				dto.setPauserNumber(rs.getInt("PAUSER_NUMBER"));
				dto.setBillingAmount(rs.getInt("BILLING_AMOUNT"));
				dto.setName(rs.getString("NAME"));
				dto.setBillingAddress(rs.getString("BILLING_ADDRESS"));
				list.add(dto);
			}
		}
		return list;
	}
}
