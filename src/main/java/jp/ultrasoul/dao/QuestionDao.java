package jp.ultrasoul.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jp.ultrasoul.dto.QuestionDto;

public class QuestionDao {
	
	/** コネクション */
	protected Connection con;
	
	public QuestionDao(Connection con) {
		this.con = con;
	}
	
	public List<QuestionDto> selectByCourseId(int courseId) throws SQLException {
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append("   select");
		sb.append("          QUESTION_ID");
		sb.append("         ,course.COURSE_ID");
		sb.append("         ,QUESTION");
		sb.append("         ,TIME");
		sb.append("         ,course.COURSE_NAME");
		sb.append("     from QUESTION");
		sb.append("     inner join COURSE");
		sb.append("     on question.COURSE_ID = course.COURSE_ID");
		sb.append("     where question.COURSE_ID = ?");
		sb.append("			order by TIME desc");
		
		try (PreparedStatement ps = con.prepareStatement(sb.toString())) {
			ps.setInt(1, courseId);
			
			List<QuestionDto> list = new ArrayList<>();
			// SQL文を実行
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				QuestionDto dto = new QuestionDto();
				dto.setQuestionId(rs.getInt("QUESTION_ID"));
				dto.setCourseId(rs.getInt("COURSE_ID"));
				dto.setQuestion(rs.getString("QUESTION"));
				dto.setTime(rs.getTimestamp("TIME"));
				dto.setCourseName(rs.getString("COURSE_NAME"));
				list.add(dto);
			}
			return list;
		}
		
	}
	
	public int insert(int courseId, String question) throws SQLException {
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append("   insert into");
		sb.append("          QUESTION");
		sb.append("         (COURSE_ID");
		sb.append("         ,QUESTION)");
		sb.append("   values (?, ?);");
		
		// クエリを実行
		try (PreparedStatement ps = con.prepareStatement(sb.toString())) {
			ps.setInt(1, courseId);
			ps.setString(2, question);
			
			return ps.executeUpdate();	
		} 
	}


}
