package jp.ultrasoul.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import jp.ultrasoul.dto.CategoryDto;

public class CategoryDao {
	
	/** コネクション */
	protected Connection conn;
	
	public CategoryDao(Connection conn) {
		this.conn = conn;
	}
	
	public List<CategoryDto> selectAll() throws Exception{
		
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append("   select");
		sb.append("          CATEGORY_ID");
		sb.append("         ,CATEGORY_NAME");
		sb.append("     from CATEGORY");
		sb.append(" order by CATEGORY_ID");
		
		List<CategoryDto> list =  new ArrayList<>();
		
		try(Statement st = conn.createStatement()){
			
			//sql文の実行
			ResultSet rs = st.executeQuery(sb.toString());
			while(rs.next()) {
				CategoryDto dto = new CategoryDto();
				dto.setCategoryId(rs.getInt("CATEGORY_ID"));
				dto.setCategoryName(rs.getString("CATEGORY_NAME"));
				list.add(dto);
			}
			return list;
		}
		
	}
	
	/**
	 * カテゴリ名を追加する
	 * @param dto　カテゴリ名
	 * @return　　 更新結果
	 * @throws SQLException　SQL例外
	 */
	public int insert(CategoryDto dto) throws SQLException {
		
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" insert into CATEGORY");
		sb.append("           (");
		sb.append("             CATEGORY_NAME");
		sb.append("           )");
		sb.append("      values");
		sb.append("           (");
		sb.append("             ?");
		sb.append("           )");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// プレースホルダーに値をセットする
			ps.setString(1, dto.getCategoryName());

			// SQLを実行する
			return ps.executeUpdate();
		}
	}
	
	public boolean hasCategoryName(String categoryName) throws SQLException{
		
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append("     select * ");
		sb.append("     from CATEGORY");
		sb.append("     where CATEGORY_NAME = ?");
		
		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする			
			ps.setString(1, categoryName);

			// SQLを実行する
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return true;
			}
			return false;
		}
	}

}
