package jp.ultrasoul.dao;

import java.sql.Connection;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import jp.ultrasoul.dto.CourseDto;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jp.ultrasoul.dto.CourseDto;

public class CourseDao {

	/** コネクション */
	protected Connection con;

	public CourseDao(Connection con) {
		this.con = con;
	}
	
	/**
	 * コースIDの最大値を取得する
	 * @return コースIDの最大値
	 * @throws SQLException　SQL例外
	 */
	public int maxCourseId() throws SQLException {
		
		//SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append("        MAX(COURSE_ID)");
		sb.append("   from COURSE");
		
		//ステートメントオブジェクトを作成する
		try (PreparedStatement ps = con.prepareStatement(sb.toString())) {

			// SQL文を実行する
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {				
				return rs.getInt("MAX(COURSE_ID)");
			}
		}
		return 0;
	}
	
	/**
	 * 自動採番で次に付与されるコースIDを取得する
	 * @return 自動採番で次に付与されるコースID
	 * @throws SQLException　SQL例外
	 */
	public int nextCourseId() throws SQLException {
		
		//SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append("        AUTO_INCREMENT");
		sb.append("   from INFORMATION_SCHEMA.TABLES");
		sb.append("   where TABLE_SCHEMA = 'CODETRAIN'");
		sb.append("   and TABLE_NAME = 'COURSE'");
		
		//ステートメントオブジェクトを作成する
		try (PreparedStatement ps = con.prepareStatement(sb.toString())) {

			// SQL文を実行する
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {				
				return rs.getInt("AUTO_INCREMENT");
			}
		}
		return 0;
	}
	
	/**
	 * コース情報を追加する
	 * @param dto　コース情報
	 * @return　　 更新件数
	 * @throws SQLException　SQL例外
	 */
	public int insert(CourseDto dto) throws SQLException {
		
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" insert into COURSE");
		sb.append("           (");
		sb.append("             COURSE_ID");
		sb.append("            ,COURSE_NAME");
		sb.append("            ,CATEGORY_ID");
		sb.append("            ,PDF_PATH");
		sb.append("            ,ESTIMATED_TIME");
		sb.append("            ,PRECONDITION");
		sb.append("            ,OVERVIEW");
		sb.append("            ,CONTENTS");
		sb.append("            ,GOAL");
		sb.append("            ,FREE_FLG");
		sb.append("            ,TOTAL_PAGES");
		sb.append("           )");
		sb.append("      values");
		sb.append("           (");
		sb.append("             ?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("           )");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = con.prepareStatement(sb.toString())) {

			// プレースホルダーに値をセットする
			ps.setInt(1, dto.getCourseId());
			ps.setString(2, dto.getCourseName());
			ps.setInt(3, dto.getCategoryId());
			ps.setString(4, dto.getPdfPath());
			ps.setInt(5, dto.getEstimatedTime());
			ps.setString(6, dto.getPrecondition());
			ps.setString(7, dto.getOverview());
			ps.setString(8, dto.getContents());
			ps.setString(9, dto.getGoal());
			ps.setBoolean(10, dto.isFreeFlg());
			ps.setInt(11, dto.getTotalPages());

			// SQLを実行する
			return ps.executeUpdate();
		}
		
	}

	/*
	 * 
	 * 
	 * COURSE_ID COURSE_NAME CATEGORY_ID PDF_PATH ESTIMATED_ TIME PRECONDITION
	 * OVERVIEW CONTENTS GORL FREE_FLG TOTAL_PAGES
	 * 
	 */

	public List<CourseDto> selectAll() throws Exception{
			
			// SQL文を作成する
			StringBuffer sb = new StringBuffer();
			sb.append("   select");
			sb.append("          COURSE_ID");
			sb.append("         ,COURSE_NAME");
			sb.append("         ,CATEGORY_ID");
			sb.append("         ,PDF_PATH");
			sb.append("         ,ESTIMATED_TIME");
			sb.append("         ,PRECONDITION");
			sb.append("         ,OVERVIEW");
			sb.append("         ,CONTENTS");
			sb.append("         ,GOAL");
			sb.append("         ,FREE_FLG");
			sb.append("         ,TOTAL_PAGES");
			sb.append("     from COURSE");
			
			List<CourseDto> list =  new ArrayList<>();
			
			try(Statement st = con.createStatement()){
				
				//sql文の実行
				ResultSet rs = st.executeQuery(sb.toString());
				while(rs.next()) {
					CourseDto dto = new CourseDto();
					dto.setCourseId(rs.getInt("COURSE_ID"));
					dto.setCourseName(rs.getString("COURSE_NAME"));
					dto.setCategoryId(rs.getInt("CATEGORY_ID"));
					dto.setPdfPath(rs.getString("PDF_PATH"));
					dto.setEstimatedTime(rs.getInt("ESTIMATED_TIME"));
					dto.setPrecondition(rs.getString("PRECONDITION"));
					dto.setOverview(rs.getString("OVERVIEW"));
					dto.setContents(rs.getString("CONTENTS"));
					dto.setGoal(rs.getString("GOAL"));
					dto.setFreeFlg(rs.getBoolean("FREE_FLG"));
					dto.setTotalPages(rs.getInt("TOTAL_PAGES"));
					list.add(dto);
				}
				return list;
			}

	}

	public CourseDto selectByCourseId(int courseId) throws SQLException {

		// SQL文の作成		
		StringBuffer sb = new StringBuffer();

		sb.append("   select");
		sb.append("          COURSE_ID");
		sb.append("         ,COURSE_NAME");
		sb.append("         ,COURSE.CATEGORY_ID");
		sb.append("         ,PDF_PATH");
		sb.append("         ,ESTIMATED_TIME");
		sb.append("         ,PRECONDITION");
		sb.append("         ,OVERVIEW");
		sb.append("         ,CONTENTS");
		sb.append("         ,GOAL");
		sb.append("         ,FREE_FLG");
		sb.append("         ,TOTAL_PAGES");
		sb.append("         ,CATEGORY.CATEGORY_NAME");
		sb.append("     from COURSE");
		sb.append("     inner join CATEGORY");
		sb.append("     on COURSE.CATEGORY_ID = CATEGORY.CATEGORY_ID");
		sb.append("     where COURSE_ID = ?");


		CourseDto dto = null;

		// ステートメントオブジェクトの作成
		try (PreparedStatement ps = con.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセット
			ps.setInt(1, courseId);

			// SQL文を実抗
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				dto = new CourseDto();

				dto.setCourseId(rs.getInt("COURSE_ID"));
				dto.setCourseName(rs.getString("COURSE_NAME"));
				dto.setCategoryId(rs.getInt("COURSE.CATEGORY_ID"));
				dto.setPdfPath(rs.getString("PDF_PATH"));
				dto.setEstimatedTime(rs.getInt("ESTIMATED_TIME"));
				dto.setPrecondition(rs.getString("PRECONDITION"));
				dto.setOverview(rs.getString("OVERVIEW"));
				dto.setContents(rs.getString("CONTENTS"));
				dto.setGoal(rs.getString("GOAL"));
				dto.setFreeFlg(rs.getBoolean("FREE_FLG"));
				dto.setTotalPages(rs.getInt("TOTAL_PAGES"));
				dto.setCategoryName(rs.getString("CATEGORY.CATEGORY_NAME"));

			}
		}
		return dto;
	}
	
	/**
	 * コース情報を更新する
	 * @param dto	コース情報
	 * @return		更新件数
	 * @throws SQLException　SQL例外
	 */
	public int update(CourseDto dto) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" update");
		sb.append("        COURSE");
		sb.append("    set");
		sb.append("          COURSE_NAME = ?");
		sb.append("         ,CATEGORY_ID = ?");
		sb.append("         ,ESTIMATED_TIME = ?");
		sb.append("         ,PRECONDITION = ?");
		sb.append("         ,OVERVIEW = ?");
		sb.append("         ,CONTENTS = ?");
		sb.append("         ,GOAL = ?");
		sb.append("         ,FREE_FLG = ?");
		sb.append("  where COURSE_ID = ?");
	
    	// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = con.prepareStatement(sb.toString())) {
			
        	// プレースホルダーに値をセットする
			ps.setString(1, dto.getCourseName());
			ps.setInt(2, dto.getCategoryId());
			ps.setInt(3, dto.getEstimatedTime());
			ps.setString(4, dto.getPrecondition());
			ps.setString(5, dto.getOverview());
			ps.setString(6, dto.getContents());
			ps.setString(7, dto.getGoal());
			ps.setBoolean(8, dto.isFreeFlg());
			ps.setInt(9, dto.getCourseId());

            // SQLを実行する
			return ps.executeUpdate();
		}
	}
	
	/**
	 * PDFを含むコース情報を更新する
	 * @param dto	PDFを含むコース情報
	 * @return		更新件数
	 * @throws SQLException　SQL例外
	 */
	public int updateWithPDF(CourseDto dto) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" update");
		sb.append("        COURSE");
		sb.append("    set");
		sb.append("          COURSE_NAME = ?");
		sb.append("         ,CATEGORY_ID = ?");
		sb.append("         ,PDF_PATH = ?");
		sb.append("         ,ESTIMATED_TIME = ?");
		sb.append("         ,PRECONDITION = ?");
		sb.append("         ,OVERVIEW = ?");
		sb.append("         ,CONTENTS = ?");
		sb.append("         ,GOAL = ?");
		sb.append("         ,FREE_FLG = ?");
		sb.append("         ,TOTAL_PAGES = ?");
		sb.append("  where COURSE_ID = ?");
	
    	// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = con.prepareStatement(sb.toString())) {
			
        	// プレースホルダーに値をセットする
			ps.setString(1, dto.getCourseName());
			ps.setInt(2, dto.getCategoryId());
			ps.setString(3, dto.getPdfPath());
			ps.setInt(4, dto.getEstimatedTime());
			ps.setString(5, dto.getPrecondition());
			ps.setString(6, dto.getOverview());
			ps.setString(7, dto.getContents());
			ps.setString(8, dto.getGoal());
			ps.setBoolean(9, dto.isFreeFlg());
			ps.setInt(10, dto.getTotalPages());
			ps.setInt(11, dto.getCourseId());

            // SQLを実行する
			return ps.executeUpdate();
		}
	}
	
	/**
	 * コースIDに紐づくコース情報を削除する
	 * @param dto　コース情報
	 * @throws SQLException　SQL例外
	 */
	public void deleteByCourseId(int courseId) throws SQLException {
		
    	// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" delete from COURSE");
		sb.append("       where COURSE_ID = ?");

    	// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = con.prepareStatement(sb.toString())) {
        	// プレースホルダーに値をセットする
			ps.setInt(1, courseId);

            // SQLを実行する
			ps.executeUpdate();
		}
	}
	
	/**
	 * 入力したコース名と同じコース名を取得する
	 * @return　比較結果
	 * @throws Exception　SQL
	 */
	public int selectByCourseName(String courseName) throws Exception{
		
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append("   select");
		sb.append("         COURSE_NAME");
		sb.append("     from COURSE");
		sb.append("    where COURSE_NAME = ?");
		
		try(PreparedStatement ps = con.prepareStatement(sb.toString())){
			
			//sql文の実行
			ps.setString(1, courseName);
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				return 1;
			} 
			return 0;
		}
		
	}

}
