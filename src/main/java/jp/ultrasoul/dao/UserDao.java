package jp.ultrasoul.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jp.ultrasoul.dto.UserDto;

public class UserDao {

	/** コネクション */
	protected Connection conn;

	public UserDao(Connection conn) {
		this.conn = conn;
	}

	public UserDto findByIdAndPassword(String id, String password) throws SQLException {

		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append("        USER_ID");
		sb.append("       ,EMAIL");
		sb.append("       ,NAME");
		sb.append("       ,STATUS");
		sb.append("       ,PAUSE_REASON");
		sb.append("       ,MANAGER_ID");
		sb.append("   from USER");
		sb.append("  where EMAIL = ?");
		sb.append("    and PASSWORD = sha2(?, 256)");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setString(1, id);
			ps.setString(2, password);

			// SQLを実行する
			ResultSet rs = ps.executeQuery();

			// 結果をDTOに詰める
			if (rs.next()) {
				UserDto user = new UserDto();
				user.setUserId(rs.getInt("USER_ID"));
				user.setEmail(rs.getString("EMAIL"));
				user.setName(rs.getString("NAME"));
				user.setStatus(rs.getString("STATUS"));
				user.setPauseReason(rs.getString("PAUSE_REASON"));
				user.setManagerId(rs.getInt("MANAGER_ID"));
				return user;
			}
			// 該当するデータがない場合はnullを返却する
			return null;
		}
	}

	public void insert(UserDto dto) throws SQLException {
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" insert into USER");
		sb.append("                (");
		sb.append("                  EMAIL");
		sb.append("                 ,PASSWORD");
		sb.append("                 ,NAME");
		sb.append("                 ,STATUS");
		sb.append("                 ,PAUSE_REASON");
		sb.append("                 ,MANAGER_ID");
		sb.append("                )");
		sb.append("           values");
		sb.append("                (");
		sb.append("                  ?");
		sb.append("                 ,sha2('pass', 256)");
		sb.append("                 ,?");
		sb.append("                 ,'active'");
		sb.append("                 ,''");
		sb.append("                 ,?");
		sb.append("                )");

		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			//プレースホルダに値をセットする
			ps.setString(1, dto.getEmail());
			ps.setString(2, dto.getName());
			ps.setInt(3, dto.getManagerId());
			
			//SQLを実行する
			ps.executeUpdate();
		}
	}
	
	public int updatePassword(int userId, String currentPassword, String newPassword) throws SQLException {
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" update");
		sb.append("        USER");
		sb.append("    set");
		sb.append("       PASSWORD = sha2(?, 256)");
		sb.append("  where USER_ID = ? and PASSWORD = sha2(?, 256)");
		
		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			// プレースホルダーに値をセットする
			ps.setString(1, newPassword);
			ps.setInt(2, userId);
			ps.setString(3, currentPassword);

			// SQLを実行する
			return ps.executeUpdate();
		}
	}
	public List<UserDto> selectByManagerId(int managerId) throws SQLException {
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append("       select"); 
		sb.append("             USER_ID");
		sb.append("            ,NAME");
		sb.append("            ,EMAIL");
		sb.append("            ,STATUS");
		sb.append("            ,PAUSE_REASON");
		sb.append("            ,MANAGER_ID");		
		sb.append("             from USER");
		sb.append("             where MANAGER_ID = ?");
		sb.append("             order by USER_ID asc");
					
		try(PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			//プレースホルダに値をセットする
			ps.setInt(1, managerId);
			List<UserDto> userList = new ArrayList<>();
			
			// SQLを実行する
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				UserDto user = new UserDto();
				user.setUserId(rs.getInt("user_id"));
				user.setEmail(rs.getString("email"));
				user.setName(rs.getString("name"));
				user.setStatus(rs.getString("status"));
				user.setPauseReason(rs.getString("pause_reason"));
				user.setManagerId(rs.getInt("manager_id"));
				userList.add(user);
			}
			return userList;

		}
		
	}
	public void update(UserDto dto) throws SQLException {
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append("       update"); 
		sb.append("             USER"); 
		sb.append("             set status = ?");
		sb.append("             , PAUSE_REASON = ?"); 
		sb.append("             where USER_ID = ?"); 
		
		try(PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			//プレースホルダに値をセットする
			if(dto.getStatus().equals("active")) {
				ps.setString(1, "rest");
				ps.setString(2, dto.getPauseReason());
				ps.setInt(3, dto.getUserId());
			} 
			else {
				ps.setString(1, "active");
				ps.setString(2, "");
				ps.setInt(3, dto.getUserId());

			}

			//SQLを実行する
			ps.executeUpdate();
			
		}
	}
	
	public void delete(UserDto dto) throws SQLException {
		//SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append(" delete from USER");
		sb.append("       where USER_ID = ?");
		
		// ステートメントオブジェクトを作成する
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
        	// プレースホルダーに値をセットする
			ps.setInt(1, dto.getUserId());
			
            // SQLを実行する
			ps.executeUpdate();
		}

	}
	public UserDto selectByUserId(int userId) throws SQLException {
		// SQL文を作成する
		StringBuffer sb = new StringBuffer();
		sb.append("       select"); 
		sb.append("             USER_ID");
		sb.append("            ,NAME");
		sb.append("            ,EMAIL");
		sb.append("            ,STATUS");
		sb.append("            ,PAUSE_REASON");
		sb.append("            ,MANAGER_ID");		
		sb.append("             from USER");
		sb.append("             where USER_ID = ?");
					
		try(PreparedStatement ps = conn.prepareStatement(sb.toString())) {
			//プレースホルダに値をセットする
			ps.setInt(1, userId);
			
			// SQLを実行する
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				UserDto user = new UserDto();
				user.setUserId(rs.getInt("user_id"));
				user.setEmail(rs.getString("email"));
				user.setName(rs.getString("name"));
				user.setStatus(rs.getString("status"));
				user.setPauseReason(rs.getString("pause_reason"));
				user.setManagerId(rs.getInt("manager_id"));
				return user;
			}
			return null;

		}
	}
}
